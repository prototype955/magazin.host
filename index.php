<?php 

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

use Shop\Router;

$filePath = dirname(__FILE__);

require($filePath . "/vendor/autoload.php");

$request = $_REQUEST;

$controller = isset($request["path"]) ? $request["path"] : '';

$Router = new Router($controller);

$Router->loadController();
