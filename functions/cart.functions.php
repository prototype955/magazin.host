<?php

use Shop\Cart;
use Shop\Database;
use Shop\Hooks;

/* Hooks */

Hooks::getHooks()->add_action('hook_post_user_login', 'hooks_cart_init_for_user');
Hooks::getHooks()->add_action('hook_post_order_send', 'cart_deactivate');
Hooks::getHooks()->add_action('hook_post_order_send', 'hooks_cart_init_for_user');

function hooks_cart_init_for_user()
{
    $Cart = cart_get();

    $data = $Cart->getData();

    $userId = users_get_current_user_id();

    $dbCart = cart_get_from_db_active_for_user($userId);

    if ($dbCart !== false) {
        $products = cart_get_products_from_db($dbCart["id"]);
    }

    if (isset($products) && !empty($products) && count($products) > 0) {
        cart_clear($Cart);
        $Cart->setData($dbCart);
        $Cart->setCartProducts($products);
        $Cart->save();
        
    } else {

        if (empty($data)) {

            $Cart->createNewCart();
        }

        $Cart->addItem($userId, "user_id");

        $id = cart_insert_to_database($Cart->getData(), $userId);
        $Cart->addItem($id, "id");

        $Cart->save();
    }
}

Hooks::getHooks()->add_action('hook_pre_user_logout', 'cart_clear');

/* *** */

function cart_get()
{
    $Cart = new Cart();

    $data = $Cart->getData();
    
    if (empty($data)) {

        $Cart->createNewCart();
        $Cart->save();

    }

    return $Cart;
}

function cart_insert_to_database($cart, $userId)
{
    if (isset($cart["id"])) {
        unset($cart["id"]);
    }

    if (isset($cart["products"])) {
        $products = $cart["products"];
        unset($cart["products"]);
    }

    $cart["user_id"] = $userId;

    Database::getDb()->query("INSERT INTO ?n SET ?u", DB_TABLE_CART, $cart);

    $newCart = cart_get_from_db_active_for_user($userId);

    $id = isset($newCart["id"]) ? $newCart["id"] : 0;

    foreach ($products as $product) {

        $cartProduct = array(
            "product_id" => $product["id"],
            "cart_id" => $id,
            "num" => $product["num"]
        );

        Database::getDb()->query("INSERT INTO ?n SET ?u", DB_TABLE_CART_PRODUCTS, $cartProduct);
    }

    return $id;
}

function cart_get_from_db_active_for_user($userId)
{
    $cart = Database::getDb()->getRow(
        "SELECT * FROM ?n WHERE active = ?i AND user_id = ?i ", 
        DB_TABLE_CART, 
        1, 
        $userId
    );

    if (!empty($cart)) {

        return $cart;
        
    } else {
        return false;
    }
}

function cart_add_product($Product, $num = 1)
{
    $Cart = cart_get();

    $Cart->addCartProduct($Product, $num);

    $productId = $Product->getId();
    
    if (users_is_authorized() === true) {
        cart_add_product_to_db($Cart, $productId, $num);
    }
}

function cart_change_num_product($productId, $num = 1)
{
    $Cart = cart_get();

    $Cart->changeNumCartProduct($productId, $num);
    
    if (users_is_authorized() === true) {
        cart_change_num_product_in_db($Cart, $productId, $num);
    }
}

function cart_change_num_product_in_db($Cart, $productId, $num)
{
    $cart = $Cart->getData();

    $cartId = $cart["id"];

    $data = array(
        "num" => $num
    );

    Database::getDb()->query("UPDATE ?n SET ?u WHERE product_id = ?i AND cart_id = ?i", DB_TABLE_CART_PRODUCTS, $data, $productId, $cartId); 
}

function cart_remove_product($productId)
{
    $Cart = cart_get();

    $Cart->removeCartProduct($productId);
    
    if (users_is_authorized() === true) {
        cart_remove_product_in_db($Cart, $productId);
    }
}

function cart_remove_product_in_db($Cart, $productId)
{
    $cart = $Cart->getData();

    $cartId = $cart["id"];

    Database::getDb()->query("DELETE FROM ?n WHERE product_id = ?i AND cart_id = ?i", DB_TABLE_CART_PRODUCTS, $productId, $cartId); 
}

function cart_add_product_to_db($Cart, $productId, $num)
{
    $cart = $Cart->getData();

    $cartId = $cart["id"];

    $product = cart_get_product_by_cart_and_product_id($cartId, $productId);

    if (empty($product)) {

        $set = array(
            "product_id" => $productId,
            "cart_id" => $cartId,
            "num" => (int) $num
        );
    
        $result = Database::getDb()->query("INSERT INTO ?n SET ?u", DB_TABLE_CART_PRODUCTS, $set);

    } else {

        $set = array(
            "num" => $product["num"] + (int) $num
        );
    
        $result = Database::getDb()->query("UPDATE ?n SET ?u WHERE product_id = ?i AND cart_id = ?i", DB_TABLE_CART_PRODUCTS, $set, $productId, $cartId);
    }

    return $result;
}

function cart_get_product_by_cart_and_product_id($cartId, $productId)
{
    $result = Database::getDb()->getRow("SELECT * FROM ?n WHERE product_id = ?i AND cart_id = ?i", DB_TABLE_CART_PRODUCTS, $productId, $cartId);

    return $result;
}

function cart_get_products_from_db($cartId)
{
    $products = Database::getDb()->getInd(
        "product_id",
        "SELECT * FROM ?n INNER JOIN ?n ON ?n.product_id = ?n.id WHERE ?n.cart_id = ?i",
        DB_TABLE_CART_PRODUCTS,
        DB_TABLE_PRODUCTS,
        DB_TABLE_CART_PRODUCTS,
        DB_TABLE_PRODUCTS,
        DB_TABLE_CART_PRODUCTS,
        $cartId
    );

    if (!empty($products)) {

        return $products;
        
    } else {
        return false;
    }
}

function cart_deactivate()
{
    $Cart = cart_get();
    $Cart->clear();
    
    if (users_is_authorized() === true) {
        cart_deactivate_in_db($Cart);
    }
}

function cart_deactivate_in_db($Cart)
{
    $cart = $Cart->getData();

    $cartId = $cart["id"];

    $data = array("active" => 0);

    Database::getDb()->query(
        "UPDATE ?n SET ?u WHERE id = ?i",
        DB_TABLE_CART,
        $data,
        $cartId
    );
}

function cart_clear()
{
    $Cart = cart_get();
    $Cart->clear();
    
    if (users_is_authorized() === true) {
        cart_clear_products_in_db($Cart);
    }
}

function cart_clear_products_in_db($Cart)
{
    $cart = $Cart->getData();

    $cartId = $cart["id"];

    Database::getDb()->query("DELETE FROM ?n WHERE cart_id = ?i", DB_TABLE_CART_PRODUCTS, $cartId); 
}
