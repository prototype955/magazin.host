<?php

use Shop\Product;
use Shop\Products;

function products_get_recommended()
{
    $Products = new Products();
    $recommendedProducts = $Products->getRecommendedProducts();

    return $recommendedProducts;
}

function products_get_by_id($id)
{
    $Product = new Product(false, $id);

    $Product->get();

    return $Product;
}
