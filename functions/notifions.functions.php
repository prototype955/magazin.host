<?php

use Shop\Notifions;

function notifions_add_notify($notifion, $page, $block)
{
    $Notifions = new Notifions($page);
    $Notifions->addNotifion($notifion, $block);
    $Notifions->save();
}

function notifions_get_notifions_for_page($page)
{
    $Notifions = new Notifions($page);
    $notifions = $Notifions->getNotifions();
    
    return $notifions;
}
