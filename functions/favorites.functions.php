<?php

use Shop\Favorite;
use Shop\Favorites;

function favorites_add_favorite($productId, $userId)
{
    try {
        $data = array(
            "product_id" => $productId,
            "user_id" => $userId
        );

        $Favorite = new Favorite($data);
        $Favorite->insert();

        return $Favorite;

    } catch (Exception $e) {

        throw $e;
    }

    return false;
} 

function favorites_remove_favorite($productId, $userId)
{
    try {

        $favorite = favorites_get_by_user_and_product_id($productId, $userId);
        $Favorite = new Favorite($favorite, $favorite["id"]);
        $Favorite->remove();

        return $Favorite;

    } catch (Exception $e) {

        throw $e;
    }

    return false;
} 

function favorites_is_favorite_exist($productId, $userId)
{
    try {
        $Favorites = new Favorites();

        $isExistInFavorites = $Favorites->isExistInFavorites($productId, $userId);

        return $isExistInFavorites;

    } catch (Exception $e) {

        throw $e;
    }

    return false;
} 

function favorites_get_by_user_and_product_id($productId, $userId)
{
    try {

        $Favorites = new Favorites();
        $favorite = $Favorites->getByProductIdAndUserId($productId, $userId);

        return $favorite;

    } catch (Exception $e) {

        throw $e;
    }

    return false;
} 

