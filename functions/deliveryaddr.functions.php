<?php

use Shop\DeliveryAddress;
use Shop\DeliveryAddressCollection;
use Shop\Database;

function deliveryaddr_for_user_update($data, $userId)
{
    try {

        $DeliveryAddress = new DeliveryAddress($data);
        $DeliveryAddress->setId($data["id"]);
        $DeliveryAddress->setDataItem("user_id", $userId);

        $whereUserId = Database::getDb()->parse(" AND user_id = ?i ", $userId);
        $isExist = $DeliveryAddress->isExistInDb($whereUserId);

        if ($isExist === true) {
            $DeliveryAddress->update(false, $whereUserId);
        }

        return $DeliveryAddress;

    } catch (Exception $e) {
        
        throw $e;
    } 

    return false;
}

function deliveryaddr_create_new_for_user($userId)
{
    $DeliveryAddress = new DeliveryAddress();
    $DeliveryAddress->setDataItem("user_id", $userId);
    $DeliveryAddress->insert();

    return $DeliveryAddress;
}

function deliveryaddr_get_for_user($userId)
{
    $DeliveryAddresses = new DeliveryAddressCollection();

    $whereUserId = Database::getDb()->parse(" WHERE user_id = ?i ", $userId);
    $DeliveryAddresses->getDataFromDatabase($whereUserId);
    $DeliveryAddresses->create();

    if ($DeliveryAddresses->getNum() < 1) {
        $DeliveryAddress = deliveryaddr_create_new_for_user($userId);

        $DeliveryAddresses = deliveryaddr_get_for_user($userId);
    }

    return $DeliveryAddresses;
}
