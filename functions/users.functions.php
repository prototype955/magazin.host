<?php

use Shop\LoginSession;
use Shop\UserRequest;
use Shop\Hooks;
use Shop\Database;

function users_is_authorized()
{
    try {

        $LoginSession = new LoginSession();
        $LoginSession->read();

        return true;

    } catch (Exception $e) {
        
        return false;
    } 

    return false;
}

function users_get_current_user_id()
{
    try {

        $LoginSession = new LoginSession();
        $LoginSession->read();
        $data = $LoginSession->getData();

        return $data["user_id"];

    } catch (Exception $e) {
        
        return false;
    } 

    return false;
}

function users_login($request)
{
    try {

        Hooks::getHooks()->do_action('hook_pre_user_login');

        $User = new UserRequest($request);
        $User->login();

        Hooks::getHooks()->do_action('hook_post_user_login');

        return $User;

    } catch (Exception $e) {
        
        throw $e;
    } 

    return false;
}

function users_logout()
{
    try {

        Hooks::getHooks()->do_action('hook_pre_user_logout');

        $LoginSession = new LoginSession();
        $LoginSession->read();
        $LoginSession->clear();

        Hooks::getHooks()->do_action('hook_post_user_logout');

        return $LoginSession;

    } catch (Exception $e) {
        
        throw $e;
    } 

    return false;
}

function users_registr($data)
{
    try {

        $User = new UserRequest($data, "registration");
        $User->register();

        return $operationResult;

    } catch (Exception $e) {
        
        throw $e;
    } 

    return false;
}

function users_update_user_data($data, $userId)
{
    try {

        $User = new UserRequest($data, "update");
        $operationResult = $User->update($userId);

        return $operationResult;

    } catch (Exception $e) {
        
        throw $e;
    } 

    return false;
}

function users_update_user_password($data, $userId)
{
    try {

        $User = new UserRequest($data, "update_password");
        $operationResult = $User->updatePassword($userId);

        return $operationResult;

    } catch (Exception $e) {
        
        throw $e;
    } 

    return false;
}

function users_get_user_email($userId)
{
    try {

        $table = DB_TABLE_USERS;
        $profile = Database::getDb()->getRow("SELECT user_email FROM ?n WHERE id = ?i", $table, $userId); 

        return $profile["user_email"];

    } catch (Exception $e) {
        
        throw $e;
    } 

    return false;
}

function users_get_user_orders($userId)
{
    try {

        $table = DB_TABLE_ORDERS;
        $orders = Database::getDb()->getAll("SELECT * FROM ?n WHERE user_id = ?i", $table, $userId); 

        return $orders;

    } catch (Exception $e) {
        
        throw $e;
    } 

    return false;
}
