<?php

use Shop\PaymentAddress;
use Shop\PaymentAddressCollection;
use Shop\Database;

function paymentaddr_for_user_update($data, $userId)
{
    try {

        $PaymentAddress = new PaymentAddress($data);
        $PaymentAddress->setId($data["id"]);
        $PaymentAddress->setDataItem("user_id", $userId);

        $whereUserId = Database::getDb()->parse(" AND user_id = ?i ", $userId);
        $isExist = $PaymentAddress->isExistInDb($whereUserId);

        if ($isExist === true) {
            $PaymentAddress->update(false, $whereUserId);
        }

        return $PaymentAddress;

    } catch (Exception $e) {
        
        throw $e;
    } 

    return false;
}

function paymentaddr_create_new_for_user($userId)
{
    $PaymentAddress = new PaymentAddress();
    $PaymentAddress->setDataItem("user_id", $userId);
    $PaymentAddress->insert();

    return $PaymentAddress;
}

function paymentaddr_get_for_user($userId)
{
    $PaymentAddresses = new PaymentAddressCollection();

    $whereUserId = Database::getDb()->parse(" WHERE user_id = ?i ", $userId);
    $PaymentAddresses->getDataFromDatabase($whereUserId);
    $PaymentAddresses->create();

            
    if ($PaymentAddresses->getNum() < 1) {
        $PaymentAddress = paymentaddr_create_new_for_user($userId);

        $PaymentAddresses = paymentaddr_get_for_user($userId);
    }

    return $PaymentAddresses;
}
