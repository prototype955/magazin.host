<?php

use Shop\Database;
use Shop\Hooks;
use Shop\OrderDeliveryAddress;
use Shop\OrderPaymentAddress;
use Shop\Order;

function orders_get_new_uid()
{
    $id = Database::getDb()->getRow("SELECT MAX(id) as uid FROM ?n", DB_TABLE_ORDERS);

    if (!empty($id["uid"])) {
        return sprintf("%06d", $id["uid"]);

    } else {
        return "000001";
    }
}

function order_send($userId, $cartId, $request)
{
    Hooks::getHooks()->do_action('hook_pre_order_send');

    $orderUid = orders_get_new_uid();

    $DeliveryAddress = new OrderDeliveryAddress($request["deliveryaddr"]);
    $DeliveryAddress->setDataItem("order_uid", $orderUid);
    $DeliveryAddress->prepareData($request["deliveryaddr"]);
    
    $PaymentAddress = new OrderPaymentAddress($request["paymentaddr"]);
    $PaymentAddress->setDataItem("order_uid", $orderUid);
    $PaymentAddress->prepareData($request["paymentaddr"]);

    $DeliveryAddress->insert();
    $PaymentAddress->insert();

    $Order = new Order();
    $Order->setDataItem("user_id", $userId);
    $Order->setDataItem("status", 1);
    $Order->setDataItem("uid", $orderUid);
    $Order->setDataItem("data", date("Y-m-d H:i:s", time()));
    $Order->setDataItem("cart_id", $cartId);
    $Order->insert();

    Hooks::getHooks()->do_action('hook_post_order_send');

    return $orderUid;
}
