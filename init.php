<?php 

use Shop\Database;
use Shop\Template;

$isAuthorized = users_is_authorized();

$currUserId = users_get_current_user_id();

if ($isAuthorized === true) {
    $table = DB_TABLE_USERS;
    $currentUserData = Database::getDb()->getRow("SELECT user_first_name, user_second_name FROM ?n WHERE id = ?i", $table, $currUserId); 
}

Template::getSmarty()->assign('instance', TEMPLATE_PATH);
Template::getSmarty()->assign('location', PROJECT_PROTOCOL . PROJECT_DOMAIN);
Template::getSmarty()->assign('authorized', $isAuthorized);
