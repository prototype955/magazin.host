<?php

define("ROOT_PATH", dirname(__FILE__));
define("CONTROLLERS_PATH", "controllers");
define("TEMPLATE_PATH", "templates/template");
define("ITEMS_PER_PAGE", 1);
define("PRODUCTS_PER_PAGE", 3);
define("DEFAULT_SHOP_CONTROLLER", 'shop');

/* Database */
define("DB_HOST", "localhost");
define("DB_LOGIN", "root");
define("DB_PASSWORD", "44fedemi");
define("DB_DATABASE", "shop");
define("DB_TABLE_PREFIX", "shop_");
define("DB_TABLE_USERS", DB_TABLE_PREFIX . "users");
define("DB_TABLE_LOGIN_SESSIONS", DB_TABLE_PREFIX . "login_sessions");
define("DB_TABLE_DELIVERY_ADDRESSES", DB_TABLE_PREFIX . "delivery_addresses");
define("DB_TABLE_PAYMENT_ADDRESSES", DB_TABLE_PREFIX . "payment_addresses");
define("DB_TABLE_FAVORITES", DB_TABLE_PREFIX . "favorites");
define("DB_TABLE_PRODUCTS", DB_TABLE_PREFIX . "products");
define("DB_TABLE_CART", DB_TABLE_PREFIX . "cart");
define("DB_TABLE_CART_PRODUCTS", DB_TABLE_PREFIX . "cart_products");
define("DB_TABLE_ORDERS", DB_TABLE_PREFIX . "orders");
define("DB_TABLE_ORDERS_DELIVERY_ADDRESSES", DB_TABLE_PREFIX . "orders_delivery_addresses");
define("DB_TABLE_ORDERS_PAYMENT_ADDRESSES", DB_TABLE_PREFIX . "orders_payment_addresses");

/* Address */
define('PROJECT_PROTOCOL', 'http://');
define('PROJECT_DOMAIN', 'magazin.host');

/* Smarty */
define("SMARTY_TEMPLATE_DIR", ROOT_PATH . "/templates/template");
define("SMARTY_COMPILE_DIR", ROOT_PATH . "/templates/template_c");
define("SMARTY_CACHE_DIR", ROOT_PATH . "/templates/cache");
define("SMARTY_CONFIG_DIR", ROOT_PATH . "/templates/config");

/* Ajax */
define("AJAX_PAGINATION", true);

/* User config */
define('EMAIL_MIN_LENGTH', 4);
define('EMAIL_MAX_LENGTH', 64);
define('PASSWORD_MIN_LENGTH', 4);

/* Login session config */
define('LOGIN_SESSION_ID', 'user');
define('LOGIN_SESSION_EXPIRETIME', 5500); //in seconds
define('TOKEN_SALT', 'Q45J55T');

/* Notify config */
define('NOTIFY_SESSION_ID', 'notifions');

/* Cart config */
define('CART_SESSION_ID', 'cart');
