<?php

namespace Shop;

class Cart extends Abstractions\SessionStorage
{
    private $cartStructure = array(
        "id" => 0,
        "user_id" => 0,
        "active" => 1,
        "products" => array()
    );

    public function __construct() 
    {
        $this->start(CART_SESSION_ID);

        $this->read();
    }

    public function read() 
    {
        $data = $this->getDataFromSession();
        $this->setData($data);
    }

    public function clear() 
    {
        $this->clearSession();
    }

    public function createNewCart()
    {
        $structure = $this->getCartStructure();
        
        $this->setData($structure);
    }

    public function getCartStructure()
    {
        return $this->cartStructure;
    }

    public function addCartProduct($Product, $num = 1)
    {
        if ($Product instanceof Product && $num > 0) {

            $cartProducts = $this->getCartProducts();
            $productId = $Product->getId();
            
            if (isset($cartProducts[$productId])) {
                $cartProducts[$productId]["num"] += $num;
            } else {
                $cartProducts[$productId] = $Product->getData();
                $cartProducts[$productId]["num"] = $num;
            }

            $this->setCartProducts($cartProducts);

            $this->save();

            return true;

        } else {
            return false;
        }
    }

    public function changeNumCartProduct($productId, $num)
    {
        if (!empty($productId) && $num > 0) {

            $cartProducts = $this->getCartProducts();
            
            if (isset($cartProducts[$productId])) {
                $cartProducts[$productId]["num"] = $num;
            }

            $this->setCartProducts($cartProducts);

            $this->save();

            return true;

        } else {
            return false;
        }
    }

    public function removeCartProduct($productId)
    {
        if (!empty($productId)) {

            $cartProducts = $this->getCartProducts();
            
            if (isset($cartProducts[$productId])) {
                unset($cartProducts[$productId]);
            }

            $this->setCartProducts($cartProducts);

            $this->save();

            return true;

        } else {
            return false;
        }
    }

    public function getCartProducts()
    {
        $data = $this->getData();

        return $data["products"];
    }

    public function setCartProducts($products)
    {
        $data = $this->getData();
        
        $data["products"] = $products;

        $this->setData($data);
    }

    public function getTableName()
    {
        return $this->tableName;
    }

    public function handleItem($item, $key) 
    {
        return $item;
    }

    public function setError($errorText) 
    {
        throw new \Exception($errorText);
    }
}
