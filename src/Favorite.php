<?php

namespace Shop;

class Favorite extends Abstractions\DatabaseUnit
{
    private $tableSheme = array(
        "product_id" => array(
            "title" => "Идентификатор товара", 
            "required" => true, 
            "not_empty" => true, 
            "in_form" => false, 
            "type" => "text",
            "input_size" => "",
            "input_row" => "",
            "placeholder" => ""
        ),
        "user_id" => array(
            "title" => "Идентификатор пользователя", 
            "required" => true, 
            "not_empty" => true, 
            "in_form" => false, 
            "type" => "text", 
            "input_size" => "",
            "input_row" => "",
            "placeholder" => ""
        ),
    );

    public function __construct($data = false, $id = false)
    {
        $this->setTableStructure($this->tableSheme);
        $this->setTableName(DB_TABLE_FAVORITES);

        if ($id !== false) {
            $this->setId($id);
        }

        if ($data !== false) {
            $this->setData($data);

        } elseif($id !== false) {

            $this->get();

        } else {
            $emptyDataset = $this->createDataset();
            $this->setData($emptyDataset);
        }
    }

    public function setError($errorText) 
    {
        throw new \Exception($errorText);
    }
}
