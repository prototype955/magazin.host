<?php

namespace Shop;

class Pagination
{
    private $numItems;

    private $numPages;

    private $currentPage;

    private $itemsPerPage;

    private $offset;

    public function __construct($numItems, $currentPage = 1, $itemsPerPage = ITEMS_PER_PAGE)
    {
        $this->setItemsPerPage($itemsPerPage);
        $this->setNumItems($numItems);

        $numPages = $this->countNumPages();

        $this->setNumPages($numPages);

        $currentPage = $this->handleCurrentPage($currentPage);

        $this->setCurrentPage($currentPage);

        $offset = $this->countOffset();

        $this->setOffset($offset);
    }

    public function getNumItems()
    {
        return $this->numItems;
    }

    public function getNumPages()
    {
        return $this->numPages;
    }

    public function getItemsPerPage()
    {
        return $this->itemsPerPage;
    }

    public function getOffset()
    {
        return $this->offset;
    }

    public function getCurrentPage()
    {
        return $this->currentPage;
    }

    private function countNumPages()
    {
        $numItems = $this->getNumItems();
        $itemsPerPage = $this->getItemsPerPage();

        $numPages = ceil($numItems / $itemsPerPage);

        return $numPages;
    }

    private function countOffset()
    {
        $currentPage = $this->getCurrentPage();
        $itemsPerPage = $this->getItemsPerPage();
        
        $offset = ($currentPage > 1) ? ($itemsPerPage * ($currentPage - 1)) : 0;

        return $offset;
    }

    private function handleCurrentPage($currentPage)
    {
        if (empty($currentPage)) {
            return 1;
        }

        $numPages = $this->getNumPages();

        if ($currentPage > $numPages) {
            return $numPages;
        }

        return $currentPage;
    }

    private function setItemsPerPage($itemsPerPage)
    {
        $this->itemsPerPage = $itemsPerPage;
    }

    private function setNumItems($num)
    {
        $this->numItems = $num;
    }

    private function setNumPages($num)
    {
        $this->numPages = $num;
    }

    private function setCurrentPage($page)
    {
        $this->currentPage = $page;
    }

    private function setOffset($offset)
    {
        $this->offset = $offset;
    }
}
