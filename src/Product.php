<?php

namespace Shop;

class Product extends Abstractions\DatabaseUnit
{
    private $tableSheme = array(
        "name" => array(
            "title" => "Наименование товара", 
            "required" => true, 
            "not_empty" => true, 
            "in_form" => true, 
            "type" => "text",
            "input_size" => "",
            "input_row" => "",
            "placeholder" => ""
        ),
        "description" => array(
            "title" => "Описание товара", 
            "required" => true, 
            "not_empty" => false, 
            "in_form" => true, 
            "type" => "text", 
            "input_size" => "",
            "input_row" => "",
            "placeholder" => ""
        ),
        "img" => array(
            "title" => "Изображение товара", 
            "required" => true, 
            "not_empty" => false, 
            "in_form" => true, 
            "type" => "text", 
            "input_size" => "",
            "input_row" => "",
            "placeholder" => ""
        ),
        "price" => array(
            "title" => "Цена товара", 
            "required" => true, 
            "not_empty" => false, 
            "in_form" => true, 
            "type" => "text", 
            "input_size" => "",
            "input_row" => "",
            "placeholder" => ""
        ),
        "recommended" => array(
            "title" => "Товар рекомендован?", 
            "required" => true, 
            "not_empty" => false, 
            "in_form" => true, 
            "type" => "text", 
            "input_size" => "",
            "input_row" => "",
            "placeholder" => ""
        )
    );

    public function __construct($data = false, $id = false)
    {
        $this->setTableStructure($this->tableSheme);
        $this->setTableName(DB_TABLE_PRODUCTS);

        if ($id !== false) {
            $this->setId($id);
        }

        if ($data !== false) {
            $this->setData($data);

        } elseif($id !== false) {

            $this->get();

        } else {
            $emptyDataset = $this->createDataset();
            $this->setData($emptyDataset);
        }
    }

    public function setError($errorText) 
    {
        throw new \Exception($errorText);
    }
}
