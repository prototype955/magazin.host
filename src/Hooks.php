<?php

namespace Shop;

class Hooks
{
    private static $hooks;

    public static function getHooks() 
    {
        if (empty(self::$hooks)) {

            self::$hooks = new \Hooks();
        }

        return self::$hooks;
    }

    private function __construct() {}
    private function __clone() {}
    private function __wakeup() {}
}
