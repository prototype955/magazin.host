<?php

namespace Shop;

class Products extends Abstractions\DatabaseUnitCollection
{
    public function __construct()
    {
        $this->setTableName(DB_TABLE_PRODUCTS);
    }

    public function produce($data) 
    {
        return new Product($data);
    }

    public function setError($errorText) 
    {
        throw new \Exception($errorText);
    }

    public function getRecommendedProducts()
    {
        $whereRecommendedSQL = \Shop\Database::getDb()->parse(" WHERE recommended = ?i ", 1);
        $products = $this->getDataFromDatabase($whereRecommendedSQL);

        return $products;
    }
}
