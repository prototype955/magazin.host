<?php

namespace Shop;

class PaymentAddressCollection extends Abstractions\DatabaseUnitCollection
{
    public function __construct() 
    {
        $this->setTableName(DB_TABLE_PAYMENT_ADDRESSES);
    }

    public function produce($data) 
    {
        return new PaymentAddress($data);
    }

    public function setError($errorText) 
    {
        throw new \Exception($errorText);
    }
}
