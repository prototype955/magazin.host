<?php

namespace Shop;

class DeliveryAddressCollection extends Abstractions\DatabaseUnitCollection
{
    public function __construct() 
    {
        $this->setTableName(DB_TABLE_DELIVERY_ADDRESSES);
    }

    public function produce($data) 
    {
        return new DeliveryAddress($data);
    }

    public function setError($errorText) 
    {
        throw new \Exception($errorText);
    }
}
