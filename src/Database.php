<?php

namespace Shop;

class Database
{
    private static $db;

    public static function getDb() 
    {
        if (empty(self::$db)) {

            $options = array(
                "user" => DB_LOGIN,
                "pass" => DB_PASSWORD,
                "db" => DB_DATABASE
            );

            self::$db = new \SafeMySql($options);
        }

        return self::$db;
    }

    private function __construct() {}
    private function __clone() {}
    private function __wakeup() {}
}
