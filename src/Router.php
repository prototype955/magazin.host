<?php

namespace Shop;

class Router
{
    private $currentController;

    private $mainController = "main";

    private $notExistController = "404";

    public function __construct($controller)
    {
        $this->setCurrentController($controller);
    }

    public function getCurrentController()
    {
        return $this->currentController;
    }

    public function getMainController()
    {
        return $this->mainController;
    }

    public function getNotExistController()
    {
        return $this->notExistController;
    }

    public function loadController()
    {
        $currentController = $this->getCurrentController();

        if ($this->isExistController($currentController) === true) {

            require(ROOT_PATH . "/" . CONTROLLERS_PATH . "/" . $currentController . ".php");

        } else {

            require(ROOT_PATH . "/" . CONTROLLERS_PATH . "/" . $this->notExistController  . ".php");
        }
    }

    private function isExistController($controller)
    {
        if (file_exists(ROOT_PATH . "/" . CONTROLLERS_PATH . "/" . $controller . ".php") === true) {

            return true;
        }

        return false;
    }

    private function setCurrentController($controller)
    {
        if (!empty($controller)) {

            $this->currentController = $controller;

        } else {
            $this->currentController = $this->mainController;
        }
    }
}
