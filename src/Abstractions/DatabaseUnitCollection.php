<?php

namespace Shop\Abstractions;

abstract class DatabaseUnitCollection
{
    private $tableName;

    private $objects;

    private $data;

    private $rowsNum;

    public abstract function produce($data);

    public abstract function setError($errorText);

    public function create()
    {
        $data = $this->getData();
        $objects = array();

        if (!empty($data)) {

            foreach ($data as $value) {

                $Object = $this->produce($value);
                $objects[] = $Object;
            }
        }

        $this->setObjects($objects);
    }

    public function getNumRows($additionalSql = '')
    {
        $table = $this->getTableName();

        $data = \Shop\Database::getDb()->getRow("SELECT COUNT(*) as num FROM ?n ?p", $table, $additionalSql);

        $num = isset($data["num"]) ? (int) $data["num"] : 0;

        $this->setRowsNum($num);

        return $num;
    }

    public function getDataFromDatabase($additionalSql = '')
    {
        $table = $this->getTableName();

        $data = \Shop\Database::getDb()->getAll("SELECT * FROM ?n ?p", $table, $additionalSql);

        $this->setData($data);

        return $data;
    }

    public function getNum()
    {
        $data = $this->data;

        return !empty($data) ? count($this->data) : 0;
    }

    public function get()
    {
        return $this->objects;
    }

    public function getTableName()
    {
        return $this->tableName;
    }

    public function getData()
    {
        return $this->data;
    }

    public function setTableName($tableName)
    {
        $this->tableName = $tableName;
    }

    protected function setData($data)
    {
        $this->data = $data;
    }

    protected function setObjects($objects)
    {
        $this->objects = $objects;
    }

    protected function setRowsNum($rowsNum)
    {
        $this->rowsNum = $rowsNum;
    }
}
