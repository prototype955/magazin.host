<?php

namespace Shop\Abstractions;

abstract class SessionStorage
{
    protected $data;

    protected $storageName;

    public abstract function handleItem($item, $key);
    public abstract function read();
    public abstract function clear();
    public abstract function setError($errorText);

    public function start($storageName)
    {
        if (session_status() !== PHP_SESSION_ACTIVE) {
            session_start();
        }

        $this->setStorageName($storageName);

        $data = $this->getDataFromSession();

        $this->setData($data);
    }

    public function addItem($item, $key = false)
    {
        $data = $this->getData();

        $item = $this->handleItem($item, $key);

        if ($key === false) {
            $data[] = $item;
            
        } else {
            $data[$key] = $item;
        }
        
        $this->setData($data);
    }

    public function removeItem($itemKey)
    {
        $data = $this->getData();

        if (isset($data[$itemKey])) {
            unset($data[$itemKey]);
        }

        $this->setData($data);
    }

    public function save($data = false)
    {
        $storageName = $this->getStorageName();

        if ($data === false) {
            $data = $this->getData();
        }

        $_SESSION[$storageName] = $data;
    }

    public function clearSession()
    {
        $sessionName = $this->getStorageName();

        if (session_status() === PHP_SESSION_ACTIVE) {
            unset($_SESSION[$sessionName]);
        }
    }

    public function getData()
    {
        return $this->data;
    }

    public function getStorageName()
    {
        return $this->storageName;
    }

    public function setData($data)
    {
        $this->data = $data;
    }

    public function getItemByKey($key)
    {
        $data = $this->getData();

        if (isset($data[$key])) {
            return $data[$key];

        } else {
            return false;
        }
    }

    protected function getDataFromSession()
    {
        $storageName = $this->getStorageName();

        if (isset($_SESSION[$storageName])) {
            return $_SESSION[$storageName];

        } else {
            return array();
        }
    }

    protected function updateItemByKey($key, $item)
    {
        $data = $this->getData();

        if (isset($data[$key])) {

            $data[$key] = $item;

        } else {

            $data[$key] = $item;
        }
    }

    protected function setStorageName($name)
    {
        $this->storageName = $name;
    }
}
