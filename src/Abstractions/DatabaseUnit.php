<?php

namespace Shop\Abstractions;

abstract class DatabaseUnit
{
    protected $tableStructure;

    protected $tableName;

    protected $data;

    protected $id;

    public abstract function setError($errorText);

    public function get($additionalSQL = '')
    {
        $id = $this->getId();
        $table = $this->getTableName();

        $data = \Shop\Database::getDb()->getRow("SELECT * FROM ?n WHERE id = ?i ?p", $table, $id, $additionalSQL);

        if (!empty($data)) {

            $this->setData($data);
            $this->setId($data["id"]);

            return $data;

        } else {
            $this->setError("Ошибка! Объект не найден!");

            return false;
        }
    }

    public function isExistInDb($additionalSQL = '')
    {
        $id = $this->getId();
        $table = $this->getTableName();

        $data = \Shop\Database::getDb()->getRow("SELECT * FROM ?n WHERE id = ?i ?p", $table, $id, $additionalSQL);

        if (empty($data)) {

            $this->setError("Ошибка! Объект не найден!");

            return false;
        }

        return true;
    }

    public function insert($data = false)
    {
        $table = $this->getTableName();

        if ($data === false) {
            $data = $this->getData();
        }

        $data = $this->prepareData($data);

        \Shop\Database::getDb()->query("INSERT INTO ?n SET ?u", $table, $data);
    }

    public function update($data = false, $additionalSQL = '')
    {
        $id = $this->getId();
        $table = $this->getTableName();

        if ($data === false) {
            $data = $this->getData();
        }

        $data = $this->prepareData($data);

        \Shop\Database::getDb()->query("UPDATE ?n SET ?u WHERE id = ?i ?p", $table, $data, $id, $additionalSQL);
    }

    public function remove()
    {
        $id = $this->getId();
        $table = $this->getTableName();

        \Shop\Database::getDb()->query("DELETE FROM ?n WHERE id = ?i", $table, $id);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getData()
    {
        return $this->data;
    }

    public function getTableName()
    {
        return $this->tableName;
    }

    public function getTableStructure()
    {
        return $this->tableStructure;
    }

    public function setData($data)
    {
        $this->data = $data;
    }

    public function setDataItem($key, $item)
    {
        $data = $this->getData();

        $data[$key] = $item;

        $this->setData($data);
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function prepareData($data)
    {
        $tableStructure = $this->getTableStructure();
        $preparedData = array();

        foreach ($tableStructure as $key => $col) {

            if (isset($data[$key])) {

                if ($col["not_empty"] === true && empty($data[$key])) {
                    $this->setError("Поле {$col["title"]} не может быть пустым!");
                }

                if (is_array($data[$key])) {
                    json_encode($data[$key]);
                }

                $preparedData[$key] = $data[$key];

            } elseif ($col["required"] === true) {
                $this->setError("Поле {$col["title"]} отсутствует в запросе!");
            }
        }

        return $preparedData;
    }

    protected function createDataset($data = array())
    {
        $tableStructure = $this->getTableStructure();
        $dataset = array();

        foreach ($tableStructure as $key => $col) {
            $dataset[$key] = isset($data[$key]) ? $data[$key] : '';
        }

        return $dataset;
    }

    protected function setTableStructure($tableStructure)
    {
        $this->tableStructure = $tableStructure;
    }

    protected function setTableName($tableName)
    {
        $this->tableName = $tableName;
    }
}
