<?php

namespace Shop;

class LoginSession extends Abstractions\SessionStorage
{
    private $tableName = DB_TABLE_LOGIN_SESSIONS;

    private $sessionSheme = array(
        "ip" => array("alias" => "ip", "required_for_session" => true),
        "token_hash" => array("alias" => "hash", "required_for_session" => true),
        "time_of_create" => array("alias" => "create_time", "required_for_session" => true),
        "user_id" => array("alias" => "user", "required_for_session" => true),
        "session_crc" => array("alias" => "crc", "required_for_session" => false)
    );

    public function __construct() 
    {
        $this->start(LOGIN_SESSION_ID);
    }

    public function read() 
    {
        $session = $this->parseDataFromSession();

        $this->setData($session);

        $sessionFromDb = $this->getSessionFromDatabase();

        if (!empty($sessionFromDb) && isset($sessionFromDb["token_hash"])) {

            if (sha1($session["token_hash"] . TOKEN_SALT) !== $sessionFromDb["token_hash"]) {

                $this->setError("Ключ сессии не соответствует сгенерированному!");
            }

        } else {
            $this->setError("Сессия не существует или уже просрочена!");
        }
    }

    public function create($userId) 
    {
        $this->clear();

        $token = md5($userId . TOKEN_SALT . time());
        $ip = $this->getIp();
        $time = time();

        $this->addItem($token, "token_hash");
        $this->addItem($ip, "ip");
        $this->addItem($userId, "user_id");
        $this->addItem($time, "time_of_create");

        $preparedForDatabase = $this->prepareDataForDatabase();
        $preparedForSession = $this->prepareDataForSession();

        $this->save($preparedForSession);
        $this->sendDataToDatabase($preparedForDatabase);

        $this->read();
    }

    public function clear() 
    {
        $data = $this->getData();

        $ip = $this->getIp();
        $userId = isset($data["user_id"]) ? $data["user_id"] : false;
        
        if ($userId !== false) {
            $this->removeDataInDatabaseByIpAndUserId($ip, $userId);
        }
        
        $this->clearSession();
    }

    public function getTableName() 
    {
        return $this->tableName;
    }

    public function handleItem($item, $key) 
    {
        return $item;
    }

    public function prepareDataForDatabase() 
    {
        $data = $this->getData();

        $requestData = array(
            "token_hash" => sha1($data["token_hash"] . TOKEN_SALT),
            "ip" => $data["ip"],
            "time_of_create" => date("Y-m-d H:i:s", $data["time_of_create"]),
            "user_id" => $data["user_id"]
        );

        $sessionCRC = $this->generateSessionCRC($data["time_of_create"], $data["user_id"], $data["ip"], $data["token_hash"]);

        $requestData["session_crc"] = $sessionCRC;

        return $requestData;
    }

    private function generateSessionCRC($timeOfCreate, $userId, $ip, $token)
    {
        return crc32($timeOfCreate . '-' . $userId . '-' . $ip . '-' . $token);
    }

    private function prepareDataForSession() 
    {
        $data = $this->getData();

        $requestData = array(
            "hash" => $data["token_hash"],
            "ip" => $data["ip"],
            "create_time" => $data["time_of_create"],
            "user" => $data["user_id"],
        );

        return $requestData;
    }

    private function parseDataFromSession()
    {
        $session = $this->getDataFromSession();
        $sessionSheme = $this->getSessionSheme();
        $data = array();

        if (!empty($session)) {

            foreach ($sessionSheme as $key => $value) {

                if (!isset($session[$value["alias"]]) && $value["required_for_session"] === true) {
                    $this->setError("В сессии не хватает параметра {$value["alias"]}. Сессия будет очищена!");
    
                    return false;
                }

                if (isset($session[$value["alias"]])) {
                    $data[$key] = $session[$value["alias"]];
                }
            }

            $sessionCRC = $this->generateSessionCRC($data["time_of_create"], $data["user_id"], $data["ip"], $data["token_hash"]);

            $data["session_crc"] = $sessionCRC;
    
            return $data;

        } else {

            $this->setError("Сессия пуста!");

            return false;
        }
    }

    private function sendDataToDatabase($data) 
    {
        $table = $this->getTableName();

        Database::getDb()->query("INSERT INTO ?n SET ?u", $table, $data);
    }

    private function removeDataInDatabaseByIpAndUserId($ip, $userId) 
    {
        $table = $this->getTableName();

        Database::getDb()->query("DELETE FROM ?n WHERE ip = ?s AND user_id = ?i", $table, $ip, $userId);
    }

    public function setError($errorText) 
    {
        $this->clear();
        throw new \Exception($errorText);
    }

    private function getIp() 
    {
        if (getenv('HTTP_X_FORWARDED_FOR')) {

            $ip = getenv('HTTP_X_FORWARDED_FOR');

        } elseif (getenv('HTTP_X_FORWARDED')) {

            $ip = getenv('HTTP_X_FORWARDED');

        } elseif (getenv('HTTP_CLIENT_IP')) {

            $ip = getenv('HTTP_CLIENT_IP');

        } elseif (getenv('REMOTE_ADDR')) {

            $ip = getenv('REMOTE_ADDR');

        } elseif (getenv('HTTP_FORWARDED')) {

            $ip = getenv('HTTP_FORWARDED');

        } elseif (getenv('HTTP_FORWARDED_FOR')) {

            $ip = getenv('HTTP_FORWARDED_FOR');

        } else {
            $ip = '127.0.0.1';
        }

        return $ip;
    }

    private function getSessionFromDatabase() 
    {
        $table = $this->getTableName();
        $data = $this->getData();
        $ip = $this->getIp();
        $expiretime = date("Y-m-d H:i:s" , time() - LOGIN_SESSION_EXPIRETIME);
        $session = Database::getDb()->getRow("SELECT * FROM ?n WHERE ip = ?s AND user_id = ?i AND time_of_create > ?s AND session_crc = ?s", $table, $ip, $data["user_id"], $expiretime, $data["session_crc"]);

        return $session;
    }

    private function getSessionSheme() 
    {
        return $this->sessionSheme;
    }
}
