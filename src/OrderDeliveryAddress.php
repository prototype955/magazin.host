<?php

namespace Shop;

class OrderDeliveryAddress extends Abstractions\DatabaseUnit
{
    private $tableSheme = array(
        "order_uid" => array(
            "title" => "Уникальный номер заказа", 
            "required" => false, 
            "not_empty" => false, 
            "in_form" => false, 
            "type" => "text", 
            "input_size" => "mini",
            "input_row" => "3",
            "placeholder" => "Если есть"
        ),
        "fio" => array(
            "title" => "ФИО получателя", 
            "required" => true, 
            "not_empty" => true, 
            "in_form" => true, 
            "type" => "text", 
            "input_size" => "medium",
            "input_row" => "1",
            "placeholder" => "Например: Смирнов Иван Сергеевич"
        ),
        "phone" => array(
            "title" => "Телефон", 
            "required" => true,
            "not_empty" => true,  
            "in_form" => true, 
            "type" => "text", 
            "input_size" => "medium",
            "input_row" => "1",
            "placeholder" => "Например: +7 (999) 99-99-99"
        ),
        "post_index" => array(
            "title" => "Почтовый индекс", 
            "required" => true, 
            "not_empty" => true, 
            "in_form" => true, 
            "type" => "text", 
            "input_size" => "small",
            "input_row" => "2",
            "placeholder" => "Индекс"
        ),
        "address" => array(
            "title" => "Адрес", 
            "required" => true,
            "not_empty" => true,  
            "in_form" => true, 
            "type" => "text", 
            "input_size" => "big",
            "input_row" => "2",
            "placeholder" => "Город, Район, Улица"
        ),
        "house" => array(
            "title" => "Номер дома", 
            "required" => true, 
            "not_empty" => true, 
            "in_form" => true, 
            "type" => "text", 
            "input_size" => "medium",
            "input_row" => "3",
            "placeholder" => "Номер дома и/или строения"
        ),
        "appartement" => array(
            "title" => "Номер квартиры/офиса", 
            "required" => false, 
            "not_empty" => false, 
            "in_form" => true, 
            "type" => "text", 
            "input_size" => "medium",
            "input_row" => "3",
            "placeholder" => "Номер квартиры/офиса"
        ),
        "security_code" => array(
            "title" => "Код домофона", 
            "required" => false, 
            "not_empty" => false, 
            "in_form" => true, 
            "type" => "text", 
            "input_size" => "mini",
            "input_row" => "3",
            "placeholder" => "Если есть"
        )
    );

    public function __construct($data = false, $id = false)
    {
        $this->setTableStructure($this->tableSheme);
        $this->setTableName(DB_TABLE_ORDERS_DELIVERY_ADDRESSES);

        if ($id !== false) {
            $this->setId($id);
        }

        if ($data !== false) {
            $this->setData($data);

        } elseif($id !== false) {

            $this->get();

        } else {
            $emptyDataset = $this->createDataset();
            $this->setData($emptyDataset);
        }
    }

    public function setError($errorText) 
    {
        throw new \Exception($errorText);
    }
}
