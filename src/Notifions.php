<?php

namespace Shop;

class Notifions extends Abstractions\SessionStorage
{
    private $page;

    public function __construct($page) 
    {
        $this->start(NOTIFY_SESSION_ID);

        $this->setPage($page);
        $this->read();
    }

    public function addNotifion($notifion, $block) 
    {
        $page = $this->getPage();
        $data = $this->getData();

        if (!isset($data[$page])) {
            $data[$page] = array($block => array());
        }

        if (!isset($data[$page][$block])) {
            $data[$page][$block] = array();
        }

        $data[$page][$block][] = $notifion;

        $this->setData($data);
    }

    public function removeNotifion($notifionKey, $block) 
    {
        $page = $this->getPage();
        $data = $this->getData();

        if (isset($data[$page][$block][$notifionKey])) {
            unset($data[$page][$block][$notifionKey]);
        }

        $this->setData($data);
    }

    public function getNotifions($remove = true, $block = false) 
    {
        $page = $this->getPage();
        $data = $this->getData();

        if ($block === false) {
            $result = isset($data[$page]) ? $data[$page] : false;

        } else {
            $result = isset($data[$page][$block]) ? $data[$page][$block] : false;
        }

        if ($remove === true) {

            if ($block === false) {
                unset($data[$page]);
    
            } else {
                unset($data[$page][$block]);
            }
            
            $this->save($data);
        }

        return $result;
    }

    public function read() 
    {
        $data = $this->getDataFromSession();
        $this->setData($data);
    }

    public function clear() 
    {
        $this->clearSession();
    }

    public function handleItem($item, $key) 
    {
        return $item;
    }

    public function setError($errorText) 
    {
        throw new \Exception($errorText);
    }

    public function getPage() 
    {
        return $this->page;
    }

    public function setPage($page) 
    {
        $this->page = $page;
    }
}
