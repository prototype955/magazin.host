<?php

namespace Shop;

class Template
{
    private static $templateHandler;

    public static function getSmarty() 
    {
        if (empty(self::$templateHandler)) {

            self::$templateHandler = new \Smarty();
            self::$templateHandler->setTemplateDir(SMARTY_TEMPLATE_DIR);
            self::$templateHandler->setCompileDir(SMARTY_COMPILE_DIR);
            self::$templateHandler->setCacheDir(SMARTY_CACHE_DIR);
            self::$templateHandler->setConfigDir(SMARTY_CONFIG_DIR);
        }

        return self::$templateHandler;
    }

    private function __construct() {}
    private function __clone() {}
    private function __wakeup() {}
}
