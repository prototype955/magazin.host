<?php

namespace Shop;

class OrderStatuses
{
    public static $statuses = array(
        1 => array("id" => 1, "name" => "Не подтвержден"),
        2 => array("id" => 2, "name" => "Подтвержден"),
        3 => array("id" => 3, "name" => "Доставляется"),
        4 => array("id" => 4, "name" => "Доставлен"),
        5 => array("id" => 5, "name" => "Завершен")
    );
    
    public static function getStatus($id)
    {
        return self::$statuses[$id];
    }
}
