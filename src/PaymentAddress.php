<?php

namespace Shop;

class PaymentAddress extends Abstractions\DatabaseUnit
{
    private $paymentAddressTableSheme = array(
        "user_id" => array(
            "title" => "Идентификатор пользователя", 
            "required" => true, 
            "not_empty" => true, 
            "in_form" => false, 
            "type" => "text",
            "input_size" => "",
            "input_row" => "",
            "placeholder" => ""
        ),
        "fio" => array(
            "title" => "ФИО держателя карты", 
            "required" => true, 
            "not_empty" => false, 
            "in_form" => true, 
            "type" => "text", 
            "input_size" => "medium",
            "input_row" => "1",
            "placeholder" => "Например: Смирнов Иван Сергеевич"
        ),
        "card_number" => array(
            "title" => "Номер карты", 
            "required" => true,
            "not_empty" => false,  
            "in_form" => true, 
            "type" => "text", 
            "input_size" => "medium",
            "input_row" => "1",
            "placeholder" => "Например: 8888 7777 6666 5555"
        ),
        "card_data" => array(
            "title" => "Срок действия карты", 
            "required" => true, 
            "not_empty" => false, 
            "in_form" => true, 
            "type" => "text", 
            "input_size" => "medium",
            "input_row" => "2",
            "placeholder" => "Например 12/20"
        ),
        "cvc_cvv" => array(
            "title" => "CVC/CVV номер", 
            "required" => true, 
            "not_empty" => false, 
            "in_form" => true, 
            "type" => "password", 
            "input_size" => "medium",
            "input_row" => "2",
            "placeholder" => "Трехзначный, указан на обратной стороне карты"
        )
    );

    public function __construct($data = false, $id = false)
    {
        $this->setTableStructure($this->paymentAddressTableSheme);
        $this->setTableName(DB_TABLE_PAYMENT_ADDRESSES);

        if ($id !== false) {
            $this->setId($id);
        }

        if ($data !== false) {
            $this->setData($data);

        } elseif($id !== false) {

            $this->get();

        } else {
            $emptyDataset = $this->createDataset();
            $this->setData($emptyDataset);
        }
    }

    public function setError($errorText) 
    {
        throw new \Exception($errorText);
    }
}
