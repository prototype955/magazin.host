<?php

namespace Shop;

class Favorites extends Abstractions\DatabaseUnitCollection
{
    public function __construct() 
    {
        $this->setTableName(DB_TABLE_FAVORITES);
    }

    public function produce($data) 
    {
        return new Favorite($data);
    }

    public function setError($errorText) 
    {
        throw new \Exception($errorText);
    }

    public function isExistInFavorites($productId, $userId)
    {
        $data = $this->getByProductIdAndUserId($productId, $userId);

        if (!empty($data)) {
            return true;
        }

        return false;
    }

    public function getByProductIdAndUserId($productId, $userId)
    {
        $table = $this->getTableName();

        $data = \Shop\Database::getDb()->getRow("SELECT * FROM ?n WHERE product_id = ?i AND user_id = ?i", $table, $productId, $userId);

        return $data;
    }
}
