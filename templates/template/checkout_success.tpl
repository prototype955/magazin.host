<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Заказ размещен | No-Name Magazin</title>

        {include file='elements/head.tpl'}
        
    </head>
    <body>

        {include file='elements/header.tpl'}
        
        <main class="display-width">
            <div class="page-navigation">
                <div class="container">
                    <div class="page-title">
                        <h2>Оформление заказа</h2>
                    </div>
                    <div class="page-path">
                        <a href="/">Главная</a>
                        <i class="fa fa-chevron-right"></i>
                        <a href="/?path=cart">Корзина</a>
                        <i class="fa fa-chevron-right"></i>
                        <p>Оформление заказа</p>
                    </div>
                </div>
            </div>
            <div class="main-content checkout-content">
                <div class="checkout-container">
                    <div class="message">
                        <h2>Благодарим вас за заказ!</h2>
                        <p>Спасибо что выбрали нас! В ближайшее время вам перезвонит (нет) менеджер для уточнения деталей.</p>
                    </div>
                </div>
                
            </div>
        </main>
        
        {include file='elements/footer.tpl'}

        {include file='elements/scripts.tpl'}

    </body>
</html>