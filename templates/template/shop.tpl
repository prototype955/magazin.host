<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Магазин | No-Name Magazin</title>
        
        {include file='elements/head.tpl'}

    </head>
    <body>
        
        {include file='elements/header.tpl'}
        
        <main class="display-width">
            <div class="page-navigation">
                <div class="container">
                    <div class="page-title">
                        <h2>Магазин</h2>
                    </div>
                    <div class="page-path">
                        <a href="/">Главная</a>
                        <i class="fa fa-chevron-right"></i>
                        <p>Магазин</p>
                    </div>
                </div>
            </div>
            <div class="main-content">
                <div class="catalog-grid">

                {if $products}

                    {foreach from=$products item=$product}

                        {include file='elements/products/grid-product-item.tpl'}

                    {/foreach}

                {else}

                <center><h3>Товаров по вашему запросу не найдено!</h3></center>

                {/if}

                </div>

            </div>

            {if $products}

                {include file='elements/items/pagination.tpl'}
                
            {/if}

        </main>
        
        {include file='elements/footer.tpl'}

        {include file='elements/scripts.tpl'}
        
    </body>
</html>