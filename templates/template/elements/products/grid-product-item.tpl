<div class="product">
    <div class="product-images-block">
        <div class="product-image">
            <a href="/?path=product&id={$product.id}" target="_blank"><img src="{$instance}/images/products/{$product.img}" /></a>
        </div>
    </div>
    <div class="product-content">
        <h4><a href="/?path=product&id={$product.id}" class="product-name-link" target="_blank">{$product.name}</a></h4>
        <p>Цена: {$product.price} <i class="fa fa-rub"></i></p>
        <a href="/?path=product&id={$product.id}" target="_blank"><div class="product-button"><i class="fa fa-bars"></i> Подробнее</div></a>
        {if isset($smarty.capture.additional_product_grid_buttons)}
            {call name=get_additional_product_grid_buttons product=$product}
        {/if}
    </div>
</div>