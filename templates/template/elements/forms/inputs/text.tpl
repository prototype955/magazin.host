<div class="input {if !empty($row.input_size)}{$row.input_size}{/if}">
    <label for="{if isset($rowId)}{$rowId}{else}{$rowKey}{/if}">{$row.title} {if $row.required === true} * {/if}</label>
    <input 
    type="text"
    id="{if isset($rowId)}{$rowId}{else}{$rowKey}{/if}" 
    name="{if isset($rowName)}{$rowName}{else}{$rowKey}{/if}" 
    placeholder="{$row.placeholder}"  
    {if isset($data[$rowKey])} value="{$data[$rowKey]}" {/if}
    {if $row.required === true} required="required" {/if}
    />
</div>