{if is_array($notifions)}

    <div class="message">

        {foreach from=$notifions item=$notify}

            <p class="notify">{$notify}</p>

        {/foreach}
    
    </div>

{/if}
