<footer>
    <div class="footer-container display-width">
        <div class="contacts">
            <div class="contact">
                <i class="fa fa-envelope"></i>
                <a href="mailto:magazin@mail.ru"> magazin@mail.ru</a>
            </div>
            <div class="contact">
                <i class="fa fa-phone"></i>
                <a href="tel:77777777"> +7 777 77-77</a>
            </div>
            <div class="contact">
                <i class="fa fa-map-marker"></i>
                <a href="#"> Москва, пр-т Ленина, д. 1</a>
            </div>
        </div>
        <div class="menu">
            <ul>
                <li><a href="/">Главная</a></li>
                <li><a href="/?path=shop">Магазин</a></li>
            </ul>
        </div>
    </div>
    <div class="footer-container display-width">
        <hr />
    </div>
    <div class="footer-container footer-social-links display-width">
        <div class="social-links">
            <div class="social-link">
                <a href="#"><i class="fa fa-facebook"></i></a>
            </div>
            <div class="social-link">
                <a href="#"><i class="fa fa-twitter"></i></a>
            </div>
            <div class="social-link">
                <a href="#"><i class="fa fa-vk"></i></a>
            </div>
        </div>
    </div>
    <div class="footer-container copyright display-width">
        <p><i class="fa fa-copyright"></i> Магазин</p>
    </div>
</footer>

<a id="toTop" onclick="animateToTop();"><i class="fa fa-chevron-up"></i></a>
<a id="chat" href="#" title="кнопка-заглушка"><i class="fa fa-envelope"></i></a>
