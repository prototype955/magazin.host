{if isset($deliveryAddresses)}

    <form action="" method="post" class="profile-form checkout-form" >

        <input type="hidden" name="path" value="checkout" />
        <input type="hidden" name="mode" value="sendOrder" />

        {if isset($errorText)}
            {include file='elements/error.tpl'}
        {/if}

        {if isset($notifions["checkout_form"])}
            {assign var="notifions" value=$notifions["checkout_form"]}
            {include file='elements/notifions.tpl'}
        {/if}

        {foreach from=$deliveryAddresses key=$k item=$address}
        <h2 class="title">Адрес доставки</h2>

        <div class="form-input-blocks">
            <div class="form-input">
                <div class="form-row">

                    {assign var="rows" value=$address->getTableStructure()}
                    {assign var="data" value=$address->getData()}

                    {assign var="formRow" value=1}

                    {foreach from=$rows key=$rowKey item=$row}

                        {if $row.in_form === true}

                            {if $row.input_row != $formRow}
                                {assign var="formRow" value=$row.input_row}
                                </div>
                                <div class="form-row">
                            {/if}

                            {if isset($request.deliveryaddr[$rowKey])}
                                {append var='data' value="`$request.deliveryaddr[$rowKey]`" index="`$rowKey`"}
                            {/if}

                            {assign var="rowId" value="deliveryaddr_`$rowKey`"}
                            {assign var="rowName" value="deliveryaddr[`$rowKey`]"}

                            {include file="elements/forms/inputs/`$row.type`.tpl"}
                        {/if}
                        
                    {/foreach}
                </div>
            </div>
        </div>
        <input type="hidden" name="deliveryaddr[id]" {if isset($data.id)} value="{$data.id}"{/if} />
        <input type="checkbox" id="rememberDelivery" name="rememberDelivery" {if isset($request.rememberDelivery)} value="{$request.rememberDelivery}"{/if} />
        <label for="rememberDelivery">Записать изменения адреса в профиль</label>
        {/foreach}

        {foreach from=$paymentAddresses key=$k item=$address}
            <h2 class="title">Адрес платежа</h2>
            <div class="form-input-blocks">
                <div class="form-input">
                    <div class="form-row">

                            {assign var="rows" value=$address->getTableStructure()}
                            {assign var="data" value=$address->getData()}

                            {assign var="formRow" value=1}

                            {foreach from=$rows key=$rowKey item=$row}

                                {if $row.in_form === true}

                                    {if $row.input_row != $formRow}
                                        {assign var="formRow" value=$row.input_row}
                                        </div>
                                        <div class="form-row">
                                    {/if}

                                    {if isset($request.paymentaddr[$rowKey])}
                                        {append var='data' value="`$request.paymentaddr[$rowKey]`" index="`$rowKey`"}
                                    {/if}
                                    
                                    {assign var="rowId" value="paymentaddr_`$rowKey`"}
                                    {assign var="rowName" value="paymentaddr[`$rowKey`]"}

                                    {include file="elements/forms/inputs/`$row.type`.tpl"}
                                {/if}
                                
                            {/foreach}
                    </div>
                </div>
            </div>
            <input type="hidden" name="paymentaddr[id]" {if isset($data.id)} value="{$data.id}"{/if} />
            <input type="checkbox" id="rememberPayment" name="rememberPayment" {if isset($request.rememberPayment)} value="{$request.rememberPayment}"{/if} />
            <label for="rememberPayment">Записать изменения адреса в профиль</label>
        {/foreach}
        
        <p>* - звездочкой отмечены поля обязательные для заполнения</p>

        <h2 class="title">Проверьте ваш заказ</h2>
        
        <div class="cart-table">
            <table class="cart-table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Изображение</th>
                        <th scope="col">Наименование</th>
                        <th scope="col">Количество</th>
                        <th scope="col">Цена / шт.</th>
                        <th scope="col">Всего</th>
                    </tr>
                </thead>
                <tbody>

                    {assign var="c" value=1}
                    {assign var="totalPrice" value=0}

                    {foreach from=$cartProducts key=$k item=$product}

                        {assign var="productTotalPrice" value=$product.price * $product.num}

                        <tr>
                            <th scope="row">{$c}</th>
                            <td>
                                <a href="/?path=product&id={$product.id}" class="product-name-link" target="_blank">
                                    <img src="{$instance}/images/products/{$product.img}" />
                                </a>
                            </td>
                            <td><a href="/?path=product&id={$product.id}" class="product-name-link" target="_blank">{$product.name}</a></td>
                            <td class="product-count-updater">
                                x {$product.num}
                            </td>
                            <td>{$product.price} <i class="fa fa-rub"></i></td>
                            <td>{$productTotalPrice} <i class="fa fa-rub"></i></td>
                        </tr>

                        {assign var="c" value=$c + 1}
                        {assign var="totalPrice" value=$totalPrice + $productTotalPrice}

                    {/foreach}

                    <tr>
                        <td colspan="3">Итого:</td>
                        <td colspan="4"><b>{$totalPrice} <i class="fa fa-rub"></i></b></td>
                    </tr>

                </tbody>
            </table>
        </div>
        
        <div class="left-right-blocks">
            <div class="left-block">
                <a class="button-2" href="/?path=cart"><i class="fa fa-chevron-left"></i> Назад</a>
            </div>
            <div class="right-block">
                <button class="button" type="submit">Подтвердить заказ <i class="fa fa-chevron-right"></i></button>
            </div>
        </div>

    </form>

{/if}
