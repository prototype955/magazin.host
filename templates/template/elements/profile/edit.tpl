{capture name="profile_content"}
    <form action="" method="post" class="profile-form" >
        <h2 class="title">Изменение данных профиля</h2>

        {assign var="errorText" value=$editError}
        {include file='elements/error.tpl'}

        {if isset($notifions.edit)}
            {assign var="notifions" value=$notifions.edit}
            {include file='elements/notifions.tpl'}
        {/if}

        <input type="hidden" name="mode" value="update_user_data" />
        <div class="form-input-blocks">
            <div class="form-input">
                <label for="firstName">Ваше имя *</label>
                <input 
                type="text" 
                id="firstName" 
                name="first_name" 
                placeholder="Например: Иван" 
                required="required" 
                value="{if isset($profile.user_first_name)}{$profile.user_first_name}{/if}" 
                />
            </div>
            <div class="form-input">
                <label for="secondName">Ваша фамилия *</label>
                <input 
                type="text" 
                id="secondName" 
                name="second_name" 
                placeholder="Например: Смирнов" 
                required="required" 
                value="{if isset($profile.user_second_name)}{$profile.user_second_name}{/if}" 
                />
            </div>
        </div>
        <p>* - звездочкой отмечены поля обязательные для заполнения</p>
        <button type="submit">Обновить</button>
    </form>
    <form action="" method="post" class="profile-form" >
        <h2 class="title">Изменение пароля</h2>

        {assign var="errorText" value=$passwordError}
        {include file='elements/error.tpl'}

        {if isset($notifions.password)}
            {assign var="notifions" value=$notifions.password}
            {include file='elements/notifions.tpl'}
        {/if}

        <input type="hidden" name="mode" value="change_password" />
        <div class="form-input-blocks">
            <div class="form-input">
                <label for="oldPassw">Ваш старый пароль *</label>
                <input type="password" id="oldPassw" name="old_password" placeholder="Ваш пароль, указанный при регистрации" required="required" />
                <label for="passw">Ваш новый пароль *</label>
                <input type="password" id="passw" name="password" placeholder="Не менее 4 символов" required="required" />
                <label for="passwRep">Повторите новый пароль *</label>
                <input type="password" id="passwRep" name="password_repeat" placeholder="Введите сюда новый пароль повторно" required="required" />
            </div>
        </div>
        <p>* - звездочкой отмечены поля обязательные для заполнения</p>
        <button type="submit">Обновить</button>
    </form>
{/capture}
