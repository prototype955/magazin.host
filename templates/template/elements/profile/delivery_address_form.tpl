{assign var="rows" value=$address->getTableStructure()}
{assign var="data" value=$address->getData()}

<form action="" method="post" class="profile-form" >
    <h2 class="title">Адрес доставки</h2>
    
    {if isset($deliveryAddressError)}
        {assign var="errorText" value=$deliveryAddressError}
        {include file='elements/error.tpl'}
    {/if}

    {if isset($notifions["delivery_address_`$data.id`"])}
        {assign var="notifions" value=$notifions["delivery_address_`$data.id`"]}
        {include file='elements/notifions.tpl'}
    {/if}

    <input type="hidden" name="mode" value="{if isset($formMode)}{$formMode}{else}update_delivery_address{/if}" />

    {if !empty($data.id)}<input type="hidden" name="id" value="{$data.id}" />{/if}

    <div class="form-input-blocks">
        <div class="form-input">
            <div class="form-row">

            {assign var="formRow" value=1}

            {foreach from=$rows key=$rowKey item=$row}

                {if $row.in_form === true}

                    {if $row.input_row != $formRow}
                        {assign var="formRow" value=$row.input_row}
                        </div>
                        <div class="form-row">
                    {/if}

                    {include file="elements/forms/inputs/`$row.type`.tpl"}
                {/if}
                
            {/foreach}

            </div>
        </div>
    </div>
    <p>* - звездочкой отмечены поля обязательные для заполнения</p>
    <button type="submit">Сохранить</button>
</form>