{capture name="profile_content"}

    {if isset($favoritesError)}
        {assign var="errorText" value=$favoritesError}
        {include file='elements/error.tpl'}
    {/if}

    {if isset($notifions.favorites)}
        {assign var="notifions" value=$notifions.favorites}
        {include file='elements/notifions.tpl'}
    {/if}

    {if isset($favorites)}

        {if !empty($favorites)}

            <div class="profile-catalog-grid">
                <div class="catalog-grid">

                    {foreach from=$favorites item=$favorite}

                        {assign var="product" value=$favorite->getData()}
                        {include file='elements/products/grid-product-item.tpl'}

                    {/foreach}

                </div>
            </div>

        {else}
            <h2 class="title">В вашем списке избранного нет ни одного товара...</h2>
        {/if}

    {/if}

{/capture}
