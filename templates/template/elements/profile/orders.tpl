{capture name="profile_content"}

    <div class="orders-table">

        {if isset($orders) && !empty($orders)}
            <table class="orders-table">
                <thead>
                    <tr>
                        <th scope="col">№</th>
                        <th scope="col">Дата</th>
                        <th scope="col">Статус</th>
                    </tr>
                </thead>
                <tbody>

                    {foreach from=$orders item=$order}
                        {assign var="status" value=Shop\OrderStatuses::getStatus($order.status)}
                        <tr>
                            <th scope="row">{$order.uid}</th>
                            <td>{$order.data|date_format:"d.m.Y"}</td>
                            <td>{$status["name"]}</td>
                        </tr>
                    {/foreach}

                </tbody>
            </table>
        {else}
            <h2 class="title">У вас пока что нет ни одного заказа</h2>
        {/if}
        
    </div> 

{/capture}
