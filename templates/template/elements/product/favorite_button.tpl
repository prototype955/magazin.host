{if $isAuthorized === true}
<div class="favorite-button">
    <a 
        href="/?path=product&mode={if $isExistInFavorites == false}addToFavorites{else}removeFromFavorites{/if}" 
        data-id="{$product.id}" 
        title="{if $isExistInFavorites == false}Добавить товар в избранное{else}Убрать товар из избранного{/if}" {if $isAuthorized === true}class="product-favorite-link"{/if}
    >
        <div class="product-favorite-button">
            <i class="fa {if $isExistInFavorites == false}fa-star-o{else}fa-star{/if}"></i>
        </div>
    </a>
    <script>
        {if isset($ajax) && $ajax == true}
        $(document).ready(function() {
            {if $isExistInFavorites == false}
                var jText = "Товар удален из избранного!";
            {else}
                var jText = "Товар добавлен в избранное!";
            {/if}
            $.jGrowl(jText);
        });
        {/if}
    </script>
</div>
{/if}