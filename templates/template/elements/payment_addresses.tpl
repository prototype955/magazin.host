<div class="addresses-form">

    {if isset($paymentAddresses)}

        {foreach from=$paymentAddresses key=$k item=$address}

            {include file="elements/profile/payment_address_form.tpl"}

        {/foreach}

    {/if}

</div>