{if isset($errorText) && !empty($errorText) }
    <div class="notice">
        <p class="error">{$errorText}</p>
    </div>
{/if}