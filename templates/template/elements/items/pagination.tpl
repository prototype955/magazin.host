{function name=pagination_link currentPage=false }
    <a 
        class="{if $smarty.const.AJAX_PAGINATION}ajax-link{/if} {if $currentPage == true}current{/if}" 
        data-element=".main-content" 
        data-target=".main-content" 
        href="/?path={$currentController}&page={$page}"
    >
    {$content}
    </a>
{/function}

{if $pages}

<div class="pagination-container">
    <ul class="pagination">

    {if $currentPage > 1}
        <li>
            {call name=pagination_link page=1 content='<i class="fa fa-angle-double-left" aria-hidden="true"></i>'}
        </li>
        <li>
            {call name=pagination_link page=($currentPage - 1) content='<i class="fa fa-angle-left" aria-hidden="true"></i>'}
        </li>
    {/if}

    {for $i=1 to $pages}

        <li>
            {call name=pagination_link page=$i currentPage=($i == $currentPage) content=$i}
        </li>

    {/for}

    {if $pages > 1 && $currentPage != $pages}
        <li>
            {call name=pagination_link page=($currentPage + 1) content='<i class="fa fa-angle-right" aria-hidden="true"></i>'}
        </li>
        <li>
            {call name=pagination_link page=$pages content='<i class="fa fa-angle-double-right" aria-hidden="true"></i>'}
        </li>
    {/if}

    </ul>
</div>

{/if}
