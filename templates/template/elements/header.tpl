<header class="display-width">
    <div class="slogan"><a href="/"><p>noname.shop</p></a></div>
    <nav class="menu">
        <ul>
            <li><a href="/">Главная</a></li>
            <li><a href="/?path=shop">Магазин</a></li>

            <li><a href="/?path=cart"><i class="fa fa-shopping-cart"></i> Корзина</a></li>

            {if $authorized === true}
                <li><a href="/?path=profile"><i class="fa fa-user"></i> Профиль</a></li>
                <li><a href="/?path=logout&mode=logout"><i class="fa fa-sign-out"></i> Выйти</a></li>
            {else}
                <li><a href="/?path=login"><i class="fa fa-sign-in"></i> Войти</a></li>
            {/if}

        </ul>
    </nav>
</header>