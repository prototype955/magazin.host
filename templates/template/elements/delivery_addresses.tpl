<div class="addresses-form">

    {if isset($deliveryAddresses)}

        {foreach from=$deliveryAddresses key=$k item=$address}

            {include file="elements/profile/delivery_address_form.tpl"}

        {/foreach}

    {/if}

</div> 