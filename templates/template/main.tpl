<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Главная | No-Name Magazin</title>

        {include file='elements/head.tpl'}
        
    </head>
    <body>

        {include file='elements/header.tpl'}
        
        <main class="display-width">
            <div class="main-slider"></div> 
            <div class="main-content">

                <div class="catalog-grid">
                    
                    <h2 class="title">Рекомендуемые товары</h2>

                    {if $recommendedProducts}
                        
                        {foreach from=$recommendedProducts item=$product}

                            {include file='elements/products/grid-product-item.tpl'}

                        {/foreach}

                    {else}
                        <center><h3>Товаров не найдено!</h3></center>
                    {/if}

                </div>
            </div>
        </main>
        
        {include file='elements/footer.tpl'}

        {include file='elements/scripts.tpl'}

    </body>
</html>