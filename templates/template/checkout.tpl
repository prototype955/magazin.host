<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Оформление заказа | No-Name Magazin</title>

        {include file='elements/head.tpl'}
        
    </head>
    <body>

        {include file='elements/header.tpl'}
        
        <main class="display-width">
            <div class="page-navigation">
                <div class="container">
                    <div class="page-title">
                        <h2>Корзина</h2>
                    </div>
                    <div class="page-path">
                        <a href="/">Главная</a>
                        <i class="fa fa-chevron-right"></i>
                        <a href="/?path=cart">Корзина</a>
                        <i class="fa fa-chevron-right"></i>
                        <p>Оформление заказа</p>
                    </div>
                </div>
            </div>
            <div class="main-content checkout-content">
                <div class="checkout-container">

                    {if $isAuthorized === true}
                        
                        {include file="elements/checkout/checkout_form.tpl"} 

                    {else}
                        <div class="notice">
                            <p>Для оформления заказа вам необходимо зарегистрироваться на сайте!   <a class="button" href="/?path=registration">Зарегистрироваться</a> <a class="button-2" href="/?path=login">У меня уже есть аккаунт</a></div>
                            </p>
                        </div>
                    {/if}

                </div>
                
            </div>
        </main>
        
        {include file='elements/footer.tpl'}

        {include file='elements/scripts.tpl'}

    </body>
</html>