<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Регистрация | No-Name Magazin</title>

        {include file='elements/head.tpl'}
        
    </head>
    <body>

        {include file='elements/header.tpl'}
        
        <main class="display-width">
            <div class="main-content">
                <form action="" method="post" class="registration-form" >
                    <h2 class="title">Регистрация</h2>
                    {include file='elements/error.tpl'}
                    <input type="hidden" name="path" value="registration" />
                    <input type="hidden" name="mode" value="register" />
                    <div class="form-input-blocks">
                        <div class="form-input">
                            <label for="firstName">Ваше имя *</label>
                            <input 
                            type="text" 
                            id="firstName" 
                            name="first_name" 
                            placeholder="Например: Иван" 
                            required="required" 
                            value="{if isset($request.first_name)}{$request.first_name}{/if}" 
                            />
                            <label for="secondName">Ваша фамилия *</label>
                            <input 
                            type="text" 
                            id="secondName" 
                            name="second_name" 
                            placeholder="Например: Смирнов" 
                            required="required" 
                            value="{if isset($request.second_name)}{$request.second_name}{/if}" 
                            />
                        </div>
                        <div class="form-input">
                            <label for="email">Ваш Email *</label>
                            <input
                            type="text" 
                            id="email"
                            name="email" 
                            placeholder="Например: ivan@mail.ru" 
                            required="required" 
                            value="{if isset($request.email)}{$request.email}{/if}" 
                            />
                            <label for="passw">Ваш пароль *</label>
                            <input type="password" id="passw" name="password" placeholder="Не менее 4 символов" required="required" />
                            <label for="passwRep">Повторите пароль *</label>
                            <input type="password" id="passwRep" name="password_repeat" placeholder="Введите сюда пароль повторно" required="required" />
                        </div>
                        <p>* - звездочкой отмечены поля обязательные для заполнения</p>
                    </div>
                    <button type="submit">ЗАРЕГИСТРИРОВАТЬСЯ</button>
                </form>
            </div>
        </main>
        
        {include file='elements/footer.tpl'}

        {include file='elements/scripts.tpl'}

    </body>
</html>