<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Корзина | No-Name Magazin</title>

        {include file='elements/head.tpl'}
        
    </head>
    <body>

        {include file='elements/header.tpl'}
        
        <main class="display-width">
            <div class="page-navigation">
                <div class="container">
                    <div class="page-title">
                        <h2>Корзина</h2>
                    </div>
                    <div class="page-path">
                        <a href="/">Главная</a>
                        <i class="fa fa-chevron-right"></i>
                        <p>Корзина</p>
                    </div>
                </div>
            </div>
            <div class="main-content cart-content">
                <div class="cart-container">
                    
                    {if isset($cartProducts) && !empty($cartProducts)}
                        <div class="cart-table">
                            <table class="cart-table">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Изображение</th>
                                        <th scope="col">Наименование</th>
                                        <th scope="col">Количество</th>
                                        <th scope="col">Цена / шт.</th>
                                        <th scope="col">Всего</th>
                                        <th scope="col">Действия</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    {assign var="c" value=1}
                                    {assign var="totalPrice" value=0}

                                    {foreach from=$cartProducts key=$k item=$product}

                                        {assign var="productTotalPrice" value=$product.price * $product.num}

                                        <tr>
                                            <th scope="row">{$c}</th>
                                            <td>
                                                <a href="/?path=product&id={$product.id}" class="product-name-link" target="_blank">
                                                    <img src="{$instance}/images/products/{$product.img}" />
                                                </a>
                                            </td>
                                            <td><a href="/?path=product&id={$product.id}" class="product-name-link" target="_blank">{$product.name}</a></td>
                                            <td class="product-count-updater">
                                                <input 
                                                    type="number" 
                                                    class="product-count-updater-input" 
                                                    data-id="{$product.id}" 
                                                    data-element=".main-content" 
                                                    data-target=".main-content" 
                                                    data-href="/?path=cart&mode=changeNumProduct"
                                                    min="1" 
                                                    value="{$product.num}" 
                                                />
                                            </td>
                                            <td>{$product.price} <i class="fa fa-rub"></i></td>
                                            <td>{$productTotalPrice} <i class="fa fa-rub"></i></td>
                                            <td class="remove-cart-product">
                                                <a 
                                                    href="/?path=cart&mode=removeProduct" 
                                                    data-id="{$product.id}" 
                                                    title="Удалить" 
                                                    data-element=".main-content"
                                                    data-target=".main-content"
                                                    class="remove-cart-product-link"
                                                >
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            </td>
                                        </tr>

                                        {assign var="c" value=$c + 1}
                                        {assign var="totalPrice" value=$totalPrice + $productTotalPrice}
                
                                    {/foreach}

                                    <tr>
                                        <td colspan="5">Итого:</td>
                                        <td colspan="2">{$totalPrice} <i class="fa fa-rub"></i></td>
                                    </tr>

                                </tbody>
                            </table>
                            <div class="left-right-blocks">
                                <div class="left-block">
                                    <a class="button-2" href="/?path=cart&mode=cartClear"><i class="fa fa-trash"></i> Очистить корзину</a>
                                </div>
                                <div class="right-block">
                                    <a class="button" href="/?path=checkout">Оформить заказ <i class="fa fa-chevron-right"></i></a>
                                </div>
                            </div>
                        </div> 
                        
                    {else}
                        <h2 class="title">В вашей корзине нет ни одного товара</h2>
                    {/if}

                </div>
                
            </div>
        </main>
        
        {include file='elements/footer.tpl'}

        {include file='elements/scripts.tpl'}

    </body>
</html>