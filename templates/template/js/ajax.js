$(document).ready(function() {

    $( '.ajax-link' ).on("click", function (e) {

        var link = $(this);
        var url = link.prop('href') ? link.prop('href') : '/';
        var element = link.data('element') ? link.data('element') : 'html';
        var target = link.data('target') ? link.data('target') : 'html';
        
        ajaxLinkRequest(url, element, target);

        e.preventDefault();
        
    });

    $( '.ajax-append-element' ).on("click", function (e) {

        var link = $(this);
        var url = link.prop('href') ? link.prop('href') : '/';
        var target = link.data('target') ? link.data('target') : 'html';
        
        ajaxAppendElement(url, target);

        e.preventDefault();
        
    });

    $( '.favorite-button' ).on("click", '.product-favorite-link', function (e) {

        var link = $(this);
        var url = link.prop('href') ? link.prop('href') : '/';
        var id = link.data('id') ? link.data('id') : '';
        var target = '.favorite-button';

        var data = {id: id};
        
        favoriteRequest(url, data, target);

        e.preventDefault();
        
    });

    $( '.main-content' ).on("click", '.remove-favorite-button-link', function (e) {

        var link = $(this);
        var url = link.prop('href') ? link.prop('href') : '/';
        var element = link.data('element') ? link.data('element') : 'html';
        var target = link.data('target') ? link.data('target') : 'html';
        var data = {};
        data.id = link.data('id') ? link.data('id') : 0;
        
        ajaxLinkRequestWithoutReplaceUrl(url, data, element, target);

        e.preventDefault();
        
    });

    $( '.add-to-cart-link' ).on("click", function (e) {

        var link = $(this);
        var url = link.prop('href') ? link.prop('href') : '/';
        var data = {};
        data.id = link.data('id') ? link.data('id') : 0;
        data.num = $('.product-count').val();
        
        cartRequest(url, data);

        e.preventDefault();
        
    });

    $( '.main-content' ).on("click", '.remove-cart-product-link', function (e) {

        var link = $(this);
        var url = link.prop('href') ? link.prop('href') : '/';
        var element = link.data('element') ? link.data('element') : 'html';
        var target = link.data('target') ? link.data('target') : 'html';
        var data = {};
        data.id = link.data('id') ? link.data('id') : 0;
        
        ajaxLinkRequestWithoutReplaceUrl(url, data, element, target);

        e.preventDefault();
        
    });

    $( '.main-content' ).on("change", '.product-count-updater-input', function (e) {

        var link = $(this);
        var url = link.data('href') ? link.data('href') : '/';
        var element = link.data('element') ? link.data('element') : 'html';
        var target = link.data('target') ? link.data('target') : 'html';
        var data = {};
        data.id = link.data('id') ? link.data('id') : 0;
        data.num = link.val() ? link.val() : 0;
        
        ajaxLinkRequestWithoutReplaceUrl(url, data, element, target);

        e.preventDefault();
        
    });

    function cartRequest(url, data)
    {
        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            success: function(response) {
                console.log(response);
                response = $.parseJSON(response);

                if (response.type == 'success') {
                    $.jGrowl(response.text);
                } else {
                    console.log(response.text);
                }
            },
            error: function() {
                console.log('Ошибка при отправке ajax запроса!');
            }
        });
    }

    function favoriteRequest(url, data, target)
    {
        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            success: function(response) {
                response = $.parseJSON(response);

                if (response.type == 'success') {
                    replaceElement($(response.text).html(), target);
                } else {
                    console.log(response.text);
                }
            },
            error: function() {
                console.log('Ошибка при отправке ajax запроса!');
            }
        });
    }

    function ajaxLinkRequest(url)
    {
        $.ajax({
            type: 'GET',
            url: url,
            success: function(content) {
                renderContent(content,);
                replaceUrl(url);
                document.title = $(content).filter('title').text();
            },
            error: function() {
                console.log('Ошибка при отправке ajax запроса!');
            }
        });
    }

    function ajaxAppendElement(url, target)
    {
        $.ajax({
            type: 'POST',
            url: url,
            success: function(content) {
                appendElement(content, target);
            },
            error: function() {
                console.log('Ошибка при отправке ajax запроса!');
            }
        });
    }

    function ajaxLinkRequestWithoutReplaceUrl(url, data, element, target)
    {
        $.ajax({
            type: 'GET',
            url: url,
            data: data,
            success: function(content) {
                renderContent(content, element, target);
            },
            error: function() {
                console.log('Ошибка при отправке ajax запроса!');
            }
        });
    }

    function replaceUrl(url)
    {
        try {
            window.history.pushState({
                path: window.location.href
            }, '', url);

        } catch(e) {
            console.error(e.stack);
            window.location.replace(url);
        }
    }

    function renderContent(content, element = 'html', target = 'html')
    {
        if (element !== 'html') {
            content = $(content).find(element).html();
        }

        if (target !== 'html') {
            $(target).html(content);
        } else {
            document.open();
            document.write(content);
            document.close();
        }
    }

    function replaceElement(element, target = 'html')
    {
        $(target).html(element);
    }

    function appendElement(content, target = 'html')
    {
        if (target !== 'html') {
            $(target).append(content);
        } else {
            document.open();
            document.write(content);
            document.close();
        }
    }
});
