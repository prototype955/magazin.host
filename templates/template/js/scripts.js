
$(document).ready(function() {

    var btn = $( '#toTop' );

    $( window ).scroll( function() {

        if ($( window ).scrollTop() > 100) {
            btn.fadeIn();
        } else {
            btn.fadeOut();
        }
    });
    
    btn.on( 'click', function(e) {
        e.preventDefault();
        $( 'html, body' ).animate( {scrollTop: 0}, '300' );
    });

});
