<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Профиль | No-Name Magazin</title>

        {include file='elements/head.tpl'}
        
    </head>
    <body>

        {include file='elements/header.tpl'}
        
        <main class="display-width">
            <div class="page-navigation">
                <div class="container">
                    <div class="page-title">
                        <h2>Профиль</h2>
                    </div>
                    <div class="page-path">
                        <a href="/">Главная</a>
                        <i class="fa fa-chevron-right"></i>
                        <p>Профиль</p>
                    </div>
                </div>
            </div>
            <ul class="profile-nav">
                <li><a class="{if $mode == 'edit'}active{/if}" href="/?path=profile&mode=edit">Изменить данные</a></li>
                <li><a class="{if $mode == 'delivery_address'}active{/if}" href="/?path=profile&mode=delivery_address">Адрес доставки</a></li>
                <li><a class="{if $mode == 'payment_address'}active{/if}" href="/?path=profile&mode=payment_address">Адрес платежа</a></li>
                <li><a class="{if $mode == 'orders'}active{/if}" href="/?path=profile&mode=orders">Заказы</a></li>
                <li><a class="{if $mode == 'favorites'}active{/if}" href="/?path=profile&mode=favorites">Избранное</a></li>
            </ul>
            <div class="main-content profile-content">
                <div class="profile-container">
                    {if isset($smarty.capture.profile_content)}
                        {$smarty.capture.profile_content}
                    {/if}
                </div>
                
            </div>
        </main>
        
        {include file='elements/footer.tpl'}

        {include file='elements/scripts.tpl'}

    </body>
</html>