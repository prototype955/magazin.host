<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Вход | No-Name Magazin</title>

        {include file='elements/head.tpl'}
        
    </head>
    <body>

        {include file='elements/header.tpl'}
        
        <main class="display-width">
            <div class="main-content">

                <form action="" method="post" class="login-form" >
                    <h2 class="title">Вход</h2>

                    {if isset($notifions.pre_form)}
                        {assign var="notifions" value=$notifions.pre_form}
                        {include file='elements/notifions.tpl'}
                    {/if}

                    {include file='elements/error.tpl'}
                    <input type="hidden" name="mode" value="login" />
                    <div class="form-input-blocks">
                        <div class="form-input">
                            <label for="email">Ваш Email</label>
                            <input 
                            type="text" 
                            id="email" 
                            name="email" 
                            placeholder="Ваш email, указанный при регистрации" 
                            required="required" 
                            value="{if isset($request.email)}{$request.email}{/if}" 
                            />
                            <label for="passw">Ваш пароль</label>
                            <input type="password" id="passw" name="password" placeholder="Ваш пароль, указанный при регистрации" required="required" />
                        </div>
                    </div>
                    <button type="submit">Войти</button>
                    <button class="button-2" onclick="location.href='{$location}/?path=registration'">Регистрация</button>
                </form>
                
            </div>
        </main>
        
        {include file='elements/footer.tpl'}

        {include file='elements/scripts.tpl'}

    </body>
</html>