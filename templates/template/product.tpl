<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>

        {if $product}

            {$product.name}

        {else}
            Пустая страница
        {/if}

        | No-Name Magazin</title>
        
        {include file='elements/head.tpl'}

    </head>
    <body>
        
        {include file='elements/header.tpl'}

        <main class="display-width">
            <div class="page-navigation">
                <div class="container">
                    <div class="page-title">
                        <h2>Магазин</h2>
                    </div>
                    <div class="page-path">
                        <a href="/">Главная</a>
                        <i class="fa fa-chevron-right"></i>
                        <a href="/?path=shop">Магазин</a>
                        <i class="fa fa-chevron-right"></i>
                        <p>

                        {if $product}

                            {$product.name}

                        {else}
                            Пустая страница
                        {/if}

                        </p>
                    </div>
                </div>
            </div>
            <div class="main-content">
                <div class="product-page-content">

                {if $product}

                    <div class="product-image">
                        <img src="{$instance}/images/products/{$product.img}" />
                    </div>
                    <div class="product-content">
                        <h2>{$product.name}</h2>
                        <h3>Цена: {$product.price} <i class="fa fa-rub"></i></h3>
                        <div class="add-to-cart">
                            <label>Количество:</label>
                            <input type="number" class="product-count" min="1" value="1" />
                            <a href="/?path=product&mode=addProductToCart" data-id="{$product.id}" title="Добавить товар в корзину" class="product-cart-link add-to-cart-link">
                                <div class="product-cart-button"><i class="fa fa-shopping-cart"></i> В корзину</div>
                            </a>
                            {include file='elements/product/favorite_button.tpl'}
                        </div>
                    </div>
                
                {else}
                    <center><h3>Товар не найден!</h3></center>
                {/if}
                    
                </div>
            </div>
            <div class="main-content block padding-top-none">
                <ul class="product-tabs">
                    <li><a class="active" href="#">Описание</a></li>
                </ul>
                <div class="product-tabs-content">
                    {$product.description}
                </div>
            </div>
        </main>
        
        {include file='elements/footer.tpl'}

        {include file='elements/scripts.tpl'}

    </body>
</html>