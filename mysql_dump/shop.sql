-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Сен 23 2020 г., 09:54
-- Версия сервера: 5.7.31-0ubuntu0.16.04.1
-- Версия PHP: 7.0.33-0ubuntu0.16.04.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `shop`
--

-- --------------------------------------------------------

--
-- Структура таблицы `shop_cart`
--

CREATE TABLE `shop_cart` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `active` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Структура таблицы `shop_cart_products`
--

CREATE TABLE `shop_cart_products` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `cart_id` int(11) NOT NULL,
  `num` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Структура таблицы `shop_delivery_addresses`
--

CREATE TABLE `shop_delivery_addresses` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `post_index` varchar(32) COLLATE utf8_bin NOT NULL,
  `address` varchar(32) COLLATE utf8_bin NOT NULL,
  `phone` varchar(32) COLLATE utf8_bin NOT NULL,
  `house` varchar(32) COLLATE utf8_bin NOT NULL,
  `appartement` varchar(32) COLLATE utf8_bin NOT NULL,
  `security_code` varchar(32) COLLATE utf8_bin NOT NULL,
  `fio` varchar(128) COLLATE utf8_bin NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Структура таблицы `shop_favorites`
--

CREATE TABLE `shop_favorites` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Структура таблицы `shop_login_sessions`
--

CREATE TABLE `shop_login_sessions` (
  `id` int(11) NOT NULL,
  `token_hash` varchar(64) COLLATE utf8_bin NOT NULL,
  `ip` varchar(64) COLLATE utf8_bin NOT NULL,
  `time_of_create` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `session_crc` varchar(32) COLLATE utf8_bin NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Структура таблицы `shop_orders`
--

CREATE TABLE `shop_orders` (
  `id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `uid` varchar(32) COLLATE utf8_bin NOT NULL,
  `data` datetime NOT NULL,
  `cart_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Структура таблицы `shop_orders_delivery_addresses`
--

CREATE TABLE `shop_orders_delivery_addresses` (
  `id` int(11) NOT NULL,
  `post_index` varchar(32) COLLATE utf8_bin NOT NULL,
  `address` varchar(32) COLLATE utf8_bin NOT NULL,
  `phone` varchar(32) COLLATE utf8_bin NOT NULL,
  `house` varchar(32) COLLATE utf8_bin NOT NULL,
  `appartement` varchar(32) COLLATE utf8_bin NOT NULL,
  `security_code` varchar(32) COLLATE utf8_bin NOT NULL,
  `fio` varchar(32) COLLATE utf8_bin NOT NULL,
  `order_uid` varchar(32) COLLATE utf8_bin NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Структура таблицы `shop_orders_payment_addresses`
--

CREATE TABLE `shop_orders_payment_addresses` (
  `id` int(11) NOT NULL,
  `fio` varchar(128) COLLATE utf8_bin NOT NULL,
  `card_number` varchar(64) COLLATE utf8_bin NOT NULL,
  `cvc_cvv` varchar(32) COLLATE utf8_bin NOT NULL,
  `card_data` varchar(32) COLLATE utf8_bin NOT NULL,
  `order_uid` varchar(32) COLLATE utf8_bin NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Структура таблицы `shop_payment_addresses`
--

CREATE TABLE `shop_payment_addresses` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `fio` varchar(128) COLLATE utf8_bin NOT NULL,
  `card_number` varchar(64) COLLATE utf8_bin NOT NULL,
  `cvc_cvv` varchar(32) COLLATE utf8_bin NOT NULL,
  `card_data` varchar(32) COLLATE utf8_bin NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Структура таблицы `shop_products`
--

CREATE TABLE `shop_products` (
  `id` int(11) NOT NULL,
  `name` varchar(64) COLLATE utf8_bin NOT NULL,
  `description` varchar(128) COLLATE utf8_bin NOT NULL,
  `img` varchar(128) COLLATE utf8_bin NOT NULL,
  `price` int(11) NOT NULL,
  `recommended` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Дамп данных таблицы `shop_products`
--

INSERT INTO `shop_products` (`id`, `name`, `description`, `img`, `price`, `recommended`) VALUES
(5, 'Ноутбук', 'Хороший ноут', 'computer.jpg', 150, 1),
(6, 'Телефон', 'телефон хороший', 'phone.jpg', 300, 0),
(7, 'Планшет', 'планшет крутой', 'surface.jpg', 800, 1),
(8, 'Тостер', 'просто тостер', 'toster.jpg', 100, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `shop_users`
--

CREATE TABLE `shop_users` (
  `id` int(11) NOT NULL,
  `user_email` varchar(128) COLLATE utf8_bin NOT NULL,
  `user_password` varchar(255) COLLATE utf8_bin NOT NULL,
  `user_first_name` varchar(128) COLLATE utf8_bin NOT NULL,
  `user_second_name` varchar(128) COLLATE utf8_bin NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `shop_cart`
--
ALTER TABLE `shop_cart`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `shop_cart_products`
--
ALTER TABLE `shop_cart_products`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `shop_delivery_addresses`
--
ALTER TABLE `shop_delivery_addresses`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `shop_favorites`
--
ALTER TABLE `shop_favorites`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `shop_login_sessions`
--
ALTER TABLE `shop_login_sessions`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `shop_orders`
--
ALTER TABLE `shop_orders`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `shop_orders_delivery_addresses`
--
ALTER TABLE `shop_orders_delivery_addresses`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `shop_orders_payment_addresses`
--
ALTER TABLE `shop_orders_payment_addresses`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `shop_payment_addresses`
--
ALTER TABLE `shop_payment_addresses`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `shop_products`
--
ALTER TABLE `shop_products`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `shop_users`
--
ALTER TABLE `shop_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `shop_cart`
--
ALTER TABLE `shop_cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `shop_cart_products`
--
ALTER TABLE `shop_cart_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `shop_delivery_addresses`
--
ALTER TABLE `shop_delivery_addresses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `shop_favorites`
--
ALTER TABLE `shop_favorites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `shop_login_sessions`
--
ALTER TABLE `shop_login_sessions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `shop_orders`
--
ALTER TABLE `shop_orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `shop_orders_delivery_addresses`
--
ALTER TABLE `shop_orders_delivery_addresses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `shop_orders_payment_addresses`
--
ALTER TABLE `shop_orders_payment_addresses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `shop_payment_addresses`
--
ALTER TABLE `shop_payment_addresses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `shop_products`
--
ALTER TABLE `shop_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `shop_users`
--
ALTER TABLE `shop_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
