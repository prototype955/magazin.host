<?php

use Shop\Template;

$request = $_REQUEST;

$mode = isset($request["mode"]) ? $request["mode"] : '';

if ($_SERVER['REQUEST_METHOD'] == 'GET') {

    if ($mode == "cartClear") {

        cart_clear();
    }

    if ($mode == "removeProduct") {

        $productId = isset($request["id"]) ? $request["id"] : 0;

        if (!empty($productId)) {

            cart_remove_product($productId);

        } else {
            $errorText = "Неверный запрос!";
        }
    }

    if ($mode == "changeNumProduct") {

        $productId = isset($request["id"]) ? $request["id"] : 0;
        $num = isset($request["num"]) ? $request["num"] : 0;

        if (!empty($productId)) {

            cart_change_num_product($productId, $num);

        } else {
            $errorText = "Неверный запрос!";
        }
    }
}

$Cart = cart_get();
$cartData = $Cart->getData();
$cartId = $cartData["id"];
$cartProducts = $Cart->getCartProducts();

Template::getSmarty()->assign('cartProducts', $cartProducts);
Template::getSmarty()->display('cart.tpl');
