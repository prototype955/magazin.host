<?php

use Shop\LoginSession;

if (users_is_authorized() === false) {
    
    header("Location: " . PROJECT_PROTOCOL . PROJECT_DOMAIN . "/?path=login");
    exit();
}

$request = $_REQUEST;

if ($request["mode"] == "logout") {

    try {

        users_logout($request);

    } catch (Exception $e) {

        $errorText = $e->getMessage() . "\n";
    } 

    header("Location: " . PROJECT_PROTOCOL . PROJECT_DOMAIN . "/?path=main");
} 

header("Location: " . PROJECT_PROTOCOL . PROJECT_DOMAIN . "/?path=main");
