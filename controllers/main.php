<?php

use Shop\Template;

$request = $_REQUEST;

$recommendedProducts = products_get_recommended();

Template::getSmarty()->assign('recommendedProducts', $recommendedProducts);

Template::getSmarty()->display('main.tpl');
