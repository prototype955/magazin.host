<?php

use Shop\Product;
use Shop\Template;
use Shop\Favorites;
use Shop\Favorite;

$request = $_REQUEST;

$isAuthorized = users_is_authorized();
$currUserId = ($isAuthorized === true) ? users_get_current_user_id() : 0;

$request = $_REQUEST;

$id = isset($request["id"]) ? (int) $request["id"] : 0;

$Product = products_get_by_id($id);

$product = $Product->getData();

$isExistInFavorites = favorites_is_favorite_exist($id, $currUserId);

Template::getSmarty()->assign('isAuthorized', $isAuthorized);
Template::getSmarty()->assign('id', $id);
Template::getSmarty()->assign('product', $product);
Template::getSmarty()->assign('isExistInFavorites', $isExistInFavorites);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    if ($request["mode"] == 'addToFavorites' && $isAuthorized == true) {

        if (!empty($product)) {

            if ($isExistInFavorites === false) {

                try {
                    favorites_add_favorite($id, $currUserId);

                    $isExistInFavorites = favorites_is_favorite_exist($id, $currUserId);
                    Template::getSmarty()->assign('isExistInFavorites', $isExistInFavorites);
                    Template::getSmarty()->assign('ajax', true);

                    $content = Template::getSmarty()->fetch('elements/product/favorite_button.tpl');

                    $favoriteAddResponse = json_encode(
                        array("type" => "success", "text" => $content)
                    );
        
                } catch (Exception $e) {

                    $favoriteAddResponse = json_encode(
                        array("type" => "error", "text" => $e->getMessage() . "\n")
                    );
                }

            } else {
                $favoriteAddResponse = json_encode(
                    array("type" => "error", "text" => 'Ошибка! Товар уже добавлен в избранное!')
                );
            }

        } else {
            $favoriteAddResponse = json_encode(
                array("type" => "error", "text" => 'Ошибка! Указанный товар не существует!')
            );
        }

        echo $favoriteAddResponse;

        exit();
    }

    if ($request["mode"] == 'removeFromFavorites' && $isAuthorized == true) {

        if (!empty($product)) {

            if ($isExistInFavorites === true) {

                try {
                    favorites_remove_favorite($id, $currUserId);

                    $isExistInFavorites = favorites_is_favorite_exist($id, $currUserId);
                    Template::getSmarty()->assign('isExistInFavorites', $isExistInFavorites);
                    Template::getSmarty()->assign('ajax', true);

                    $content = Template::getSmarty()->fetch('elements/product/favorite_button.tpl');

                    $favoriteRemoveResponse = json_encode(
                        array("type" => "success", "text" => $content)
                    );
        
                } catch (Exception $e) {

                    $favoriteRemoveResponse = json_encode(
                        array("type" => "error", "text" => $e->getMessage() . "\n")
                    );
                }

            } else {
                $favoriteRemoveResponse = json_encode(
                    array("type" => "error", "text" => 'Ошибка! Товар не добавлен в избранное!')
                );
            }

        } else {
            $favoriteRemoveResponse = json_encode(
                array("type" => "error", "text" => 'Ошибка! Указанный товар не существует!')
            );
        }

        echo $favoriteRemoveResponse;

        exit();
    }

    if ($request["mode"] == "addProductToCart" && !empty($product["id"])) {

        if (!empty($Product)) {

            $num = isset($request["num"]) ? (int) $request["num"] : 1;

            cart_add_product($Product, $num);

            $response = json_encode(
                array("type" => "success", "text" => 'Товар добавлен в корзину')
            );

        } else {
            $response = json_encode(
                array("type" => "error", "text" => 'Неверный запрос!')
            );
        }

        echo $response;

        exit();
    }
}

Template::getSmarty()->display('product.tpl');
