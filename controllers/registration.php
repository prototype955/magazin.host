<?php

use Shop\UserRequest;
use Shop\Template;

if (users_is_authorized() === true) {
    
    header("Location: " . PROJECT_PROTOCOL . PROJECT_DOMAIN . "/?path=main");
    exit();
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $request = $_REQUEST;

    if ($request["mode"] == "register") {

        try {

            users_registr($request);
            
            notifions_add_notify("Вы успешно зарегистрировались. Зайдите, используя свои email и пароль.", "login", "pre_form");

            header("Location: " . PROJECT_PROTOCOL . PROJECT_DOMAIN . "/?path=login");
    
        } catch (Exception $e) {
    
            $errorText = $e->getMessage() . "\n";
        } 
    }  
}

$request = $_REQUEST;

$errorText = isset($errorText) ? $errorText : '';

Template::getSmarty()->assign('errorText', $errorText);
Template::getSmarty()->assign('request', $request);

Template::getSmarty()->display('registration.tpl');
