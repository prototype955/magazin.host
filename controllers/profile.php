<?php

use Shop\UserRequest;
use Shop\Database;
use Shop\Template;
use Shop\DeliveryAddress;
use Shop\DeliveryAddressCollection;
use Shop\PaymentAddress;
use Shop\PaymentAddressCollection;
use Shop\Favorites;
use Shop\Favorite;
use Shop\Product;

if (users_is_authorized() === false) {
    
    header("Location: " . PROJECT_PROTOCOL . PROJECT_DOMAIN . "/?path=login");
    exit();
}

$request = $_REQUEST;
$currUserId = users_get_current_user_id();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    if ($request["mode"] == "update_user_data") {

        try {
            $userId = users_get_current_user_id();

            $operationResult = users_update_user_data($request, $userId);

            notifions_add_notify("Данные успешно изменены.", "profile", "edit");
    
        } catch (Exception $e) {
    
            $editError = $e->getMessage() . "\n";
        } 

        $request["mode"] = 'edit';
    } 
    
    if ($request["mode"] == "change_password") {

        try {
            $userId = users_get_current_user_id();

            $operationResult = users_update_user_password($request, $userId);
            
            notifions_add_notify("Пароль успешно изменен.", "profile", "password");

        } catch (Exception $e) {
    
            $passwordError = $e->getMessage() . "\n";
        } 

        $request["mode"] = 'edit';
    }

    if ($request["mode"] == "update_delivery_address") {

        if (isset($request["id"])) {

            try {
                deliveryaddr_for_user_update($request, $currUserId);
                
                notifions_add_notify("Адрес доставки успешно изменен.", "profile", "delivery_address_" . $request["id"]);
    
            } catch (Exception $e) {
        
                $deliveryAddressError = $e->getMessage() . "\n";

                Template::getSmarty()->assign('deliveryAddressError', $deliveryAddressError);
            }
    
            $request["mode"] = 'delivery_address';
        }
    }

    if ($request["mode"] == "update_payment_address") {

        if (isset($request["id"])) {

            try {
                paymentaddr_for_user_update($request, $currUserId);
                
                notifions_add_notify("Адрес платежа успешно изменен.", "profile", "payment_address_" . $request["id"]);
    
            } catch (Exception $e) {
        
                $paymentAddressError = $e->getMessage() . "\n";

                Template::getSmarty()->assign('paymentAddressError', $paymentAddressError);
            }
    
            $request["mode"] = 'payment_address';
        }
    }
}

if (isset($request["mode"]) &&  $request["mode"] == 'removeProductFromFavorites') {

    $id = isset($request["id"]) ? (int) $request["id"] : 0;

    $product = products_get_by_id($id);

    $isExistInFavorites = favorites_is_favorite_exist($id, $currUserId);

    if (!empty($product)) {

        if ($isExistInFavorites === true) {

            try {
                favorites_remove_favorite($id, $currUserId);

                notifions_add_notify("Товар удален из избранного", "profile", "favorites");
    
            } catch (Exception $e) {

                $favoritesError .= $e->getMessage() . "\n";
            }

        } else {
            $favoritesError .= 'Ошибка! Товар не добавлен в избранное!';
        }

    } else {
        $favoritesError .= 'Ошибка! Указанный товар не существует!';
    }

    $request["mode"] = 'favorites';
}

$notifions = notifions_get_notifions_for_page("profile");
$mode = isset($request["mode"]) ? $request["mode"] : 'edit';

Template::getSmarty()->assign('notifions', $notifions);
Template::getSmarty()->assign('request', $request);
Template::getSmarty()->assign('mode', $mode);

if ($mode == 'edit') {

    if ($currUserId !== false) {
        $table = DB_TABLE_USERS;
        $profile = Database::getDb()->getRow("SELECT user_first_name, user_second_name FROM ?n WHERE id = ?i", $table, $currUserId); 

        Template::getSmarty()->assign('editError', isset($editError) ? $editError : '');
        Template::getSmarty()->assign('passwordError', isset($passwordError) ? $passwordError : '');
        Template::getSmarty()->assign('profile', $profile);
        Template::getSmarty()->display('elements/profile/edit.tpl');
    }
}

if ($mode == 'delivery_address') {

    if ($currUserId !== false) {
        $DeliveryAddresses = deliveryaddr_get_for_user($currUserId);

        $deliveryAddresses = $DeliveryAddresses->get();

        Template::getSmarty()->assign('deliveryAddresses', $deliveryAddresses);
        Template::getSmarty()->display('elements/profile/delivery_addresses.tpl');
    }
}

if ($mode == 'payment_address') {

    if ($currUserId !== false) {
        $PaymentAddresses = paymentaddr_get_for_user($currUserId);

        $paymentAddresses = $PaymentAddresses->get();

        Template::getSmarty()->assign('paymentAddresses', $paymentAddresses);
        Template::getSmarty()->display('elements/profile/payment_addresses.tpl');
    }
}

if ($mode == 'orders') {
    $userId = users_get_current_user_id();
    $orders = users_get_user_orders($userId);
    Template::getSmarty()->assign('orders', $orders);
    Template::getSmarty()->display('elements/profile/orders.tpl');
}

if ($mode == 'favorites') {
    $Favorites = new Favorites();

    $whereUserId = Database::getDb()->parse(" INNER JOIN ?n ON ?n.id = ?n.product_id WHERE ?n.user_id = ?i ", DB_TABLE_PRODUCTS, DB_TABLE_PRODUCTS, DB_TABLE_FAVORITES,  DB_TABLE_FAVORITES, $currUserId);
    $Favorites->getDataFromDatabase($whereUserId);

    $Favorites->create();

    $favorites = $Favorites->get();

    Template::getSmarty()->display('elements/profile/favorites/remove_button.tpl');

    Template::getSmarty()->assign('favorites', $favorites);
    Template::getSmarty()->display('elements/profile/favorites.tpl');
}

Template::getSmarty()->display('profile.tpl');
