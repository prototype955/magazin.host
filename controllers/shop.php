<?php

use Shop\Products;
use Shop\Pagination;
use Shop\Template;
use Shop\Database;

$request = $_REQUEST;

$currentPage = isset($request["page"]) ? (int) $request["page"] : 1;

$Products = new Products();

$num = $Products->getNumRows();

$Pagination = new Pagination($num, $currentPage, PRODUCTS_PER_PAGE);

$pages = $Pagination->getNumPages();

$currentPage = $Pagination->getCurrentPage();

$productsPerPage = PRODUCTS_PER_PAGE;

$offset = $Pagination->getOffset();

$paginationSQL = Database::getDb()->parse(" LIMIT ?i, ?i ", $offset, $productsPerPage);

$products = $Products->getDataFromDatabase($paginationSQL);

Template::getSmarty()->assign('pages', $pages);
Template::getSmarty()->assign('currentPage', $currentPage);
Template::getSmarty()->assign('productsPerPage', $productsPerPage);
Template::getSmarty()->assign('offset', $offset);
Template::getSmarty()->assign('products', $products);
Template::getSmarty()->assign('currentController', $currentController);

Template::getSmarty()->display('shop.tpl');
