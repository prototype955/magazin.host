<?php

use Shop\UserRequest;
use Shop\Template;

if (users_is_authorized() === true) {
    header("Location: " . PROJECT_PROTOCOL . PROJECT_DOMAIN . "/?path=main");
    exit();
}

$request = $_REQUEST;

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    if ($request["mode"] == "login") {

        try {

            users_login($request);

            header("Location: " . PROJECT_PROTOCOL . PROJECT_DOMAIN . "/?path=main");
    
        } catch (Exception $e) {
    
            $errorText = $e->getMessage() . "\n";
        } 
    }  
}

$notifions = notifions_get_notifions_for_page("login");
$errorText = isset($errorText) ? $errorText : '';

Template::getSmarty()->assign('request', $request);
Template::getSmarty()->assign('notifions', $notifions);
Template::getSmarty()->assign('errorText', $errorText);

Template::getSmarty()->display('login.tpl');
