<?php

use Shop\Template;
use Shop\OrderDeliveryAddress;
use Shop\OrderPaymentAddress;
use Shop\Order;

$Cart = cart_get();
$cartData = $Cart->getData();
$cartId = $cartData["id"];
$cartProducts = $Cart->getCartProducts();

$request = $_REQUEST;

Template::getSmarty()->assign('cartProducts', $cartProducts);
Template::getSmarty()->assign('request', $request);

$mode = isset($request["mode"]) ? $request["mode"] : '';
$isAuthorized = users_is_authorized();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    if ($mode == "sendOrder" && $isAuthorized === true) {

        try {

            $currUserId = users_get_current_user_id();
            $userEmail = users_get_user_email($currUserId);

            $orderUid = order_send($currUserId, $cartId, $request);

            if (isset($request["rememberDelivery"]) && $request["rememberDelivery"]) {
                deliveryaddr_for_user_update($request["deliveryaddr"], $currUserId);
            }

            if (isset($request["rememberPayment"]) && $request["rememberPayment"]) {
                paymentaddr_for_user_update($request["paymentaddr"], $currUserId);
            }

            $mailSubject = 'Ваш заказ принят - noname.Magazin';
            $mailText = "Ваш заказ №{$orderUid} принят. Ожидайте звонка оператора.";

            $headers = "To: <{$userEmail}>\r\n";
            $headers .= "From: Noname.magazin <noreply@magazin.host>\r\n";

            mail($userEmail, $mailSubject, $mailText, $headers);

            Template::getSmarty()->display('checkout_success.tpl');
            
            exit();
    
        } catch (Exception $e) {
            
            $errorText = $e->getMessage() . "\n";
            Template::getSmarty()->assign('errorText', $errorText);
        } 
    }
}

if (empty($cartProducts)) {
    header("Location: " . PROJECT_PROTOCOL . PROJECT_DOMAIN . "/?path=cart");
    exit();
}

Template::getSmarty()->assign('isAuthorized', $isAuthorized);

if ($isAuthorized === true) {

    $currUserId = users_get_current_user_id();
    $DeliveryAddresses = deliveryaddr_get_for_user($currUserId);

    $deliveryAddresses = $DeliveryAddresses->get();

    Template::getSmarty()->assign('deliveryAddresses', $deliveryAddresses);

    $PaymentAddresses = paymentaddr_get_for_user($currUserId);

    $paymentAddresses = $PaymentAddresses->get();

    Template::getSmarty()->assign('paymentAddresses', $paymentAddresses);
}

Template::getSmarty()->display('checkout.tpl');
